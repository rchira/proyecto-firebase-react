export const PATH={
    PARTEDIARIO:"/app/parteDiarios",
    SINCRONIZACION:"/sincronizacion",
    INICIARSESION:"/login",
    REGISTRARRENDIMIENTO:"/app/parteDiarios/registrarRendimiento",
    CREARPARTEDIARIO:"/app/crearParteDiario",
    SINCRONIZARCOLECCION:"/app/sincronizarcoleccion",
}