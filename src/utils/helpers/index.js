import { format } from "date-fns";

export function removeDuplicates(originalArray, prop) {
 try {
  var newArray = [];
  var lookupObject = {};

  for (var i in originalArray) {
    lookupObject[originalArray[i][prop]] = originalArray[i];
  }

  for (i in lookupObject) {
    newArray.push(lookupObject[i]);
  }
  return newArray;
 } catch (error) {
   
 }
}

export function aleatorio(inferior, superior) {
  var numPosibilidades = superior - inferior;
  var aleatorio = Math.random() * (numPosibilidades + 1);
  aleatorio = Math.floor(aleatorio);
  return inferior + aleatorio;
}

export function validarEmail(valor) {
  let status = false
  if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)) {
    status = true
  }
  return status
}

export function getUser(response) {
  let user = [];
  user = localStorage.getItem("user")
    ? (user = localStorage.getItem("user"))
    : [];
  user = user.length > 0 ? JSON.parse(localStorage.getItem("user")) : [];
  if (user && user.length > 0) {
    response(user[0]);
  }
};

export function getIdAutogenerado() {
  let timeStampInMs = window.performance && window.performance.now && window.performance.timing && window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();
  timeStampInMs = timeStampInMs.toString().substring(0, 10)
  return timeStampInMs
}



  /*
  
  function geo_success(position) {
    console.log(position.coords.latitude, position.coords.longitude);
  }

  function geo_error() {
    alert("Sorry, no position available.");
  }

  var geo_options = {
    enableHighAccuracy: true,
    maximumAge: 30000,
    timeout: 27000
  };

export const ubicacionReal = navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);

  */

  export function obtenerUbicacion(resolve){
    if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(position => {
      resolve({estaPermitido:true,posicion:[position.coords.latitude, position.coords.longitude]})
    }, e => {
      resolve({estaPermitido:false,posicion:[]})
    });
    
    var watchID = navigator.geolocation.watchPosition(function(position) {
     resolve({estaPermitido:true,posicion:[position.coords.latitude, position.coords.longitude]})
    });
    navigator.geolocation.clearWatch(watchID);
    }
  }





export function hayConeccion(){
  const online = window.navigator.onLine;
  return online
}

export function formatearLoteIdAElementoPep(loteid){
  let cadena=''
  let iterador=1
  for (let i = 0; i < loteid.length; i++) {
    const element = loteid[i];
    if(iterador % 2 ==0 && iterador!==8 ){
      cadena+=element+'.'

    }else{
      cadena+=element
    }
      iterador++
  }
  return cadena
    
}

export function alNavegador(nombre,datos){
  localStorage.setItem(nombre,JSON.stringify(datos))
}
export function delNavegador(nombre){
  return JSON.parse(localStorage.getItem(nombre))
}

export function uuidv4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}


export default function colocarScroll(top,left,behavior){
  window.scroll({
    top: top,
    left: left,
    behavior: behavior,
  });
}

export function obtenerFechaActual(){
  return format(new Date(),'yyyy-MM-dd')
}

export function obtenerCantidadHoras(fechaInicio){
  let fechaGuardada = fechaInicio.toDate();
      let fechaActual = new Date();
      let diferencia = Math.abs(fechaActual - fechaGuardada);
      let horas = parseInt(diferencia / (1000 * 3600));
      return horas
}

export function estaOpcionExisteEnElObjeto(obj,key){
  return obj.hasOwnProperty(key);
}