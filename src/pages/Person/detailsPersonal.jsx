import { Grid, Typography } from "@mui/material";
import { useLocation } from "react-router-dom";
import AddTask from '@mui/icons-material/AddTask'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
  import { Line } from 'react-chartjs-2';
  import faker from 'faker';
  
  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );
  
  

export function DetailsPersonal() {

    const { state } = useLocation()
    let personal = state.datapesl;
 
    // start char
     const options = {
        responsive: true,
        plugins: {
          legend: {
            position: 'left'
          },
          title: {
            display: true,
            text: `Rendimento de  ${personal.personal}`,
          },
        },
      };
      
      const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July','Agosto'];
      
       const data = {
        labels,
        datasets: [
          {
            label: 'Dataset 1',
            data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
            borderColor: 'rgb(255, 99, 132)',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
          },
          {
            label: 'Dataset 2',
            data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
            borderColor: 'rgb(53, 162, 235)',
            backgroundColor: 'rgba(53, 162, 235, 0.5)',
          },
        ],
      };
    
    // finish char
    return (<>
        <>
            <Typography textAlign={'left'} style={{ color: '#717873' }} variant="h5" component="h5">

                <AddTask /> PROGRESO DE {(personal.personal)}

            </Typography>
            <Grid container spacing={2}>
               
                <Grid item xs={12} md={1}>

                    <strong style={{marginRight:10}}>D.N.I:</strong>
                    <label>{personal.numero_documento}</label>
                </Grid>

            </Grid>
            <Line options={options} data={data} />
        </>
        {/* https://www.npmjs.com/package/react-chartjs-2 */}
    </>)
}