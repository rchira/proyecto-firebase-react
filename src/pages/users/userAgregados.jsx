


import { useEffect, useState } from 'react';
import { Alert, AlertTitle, Container, Grid, Typography } from "@mui/material";
import UserCard from "./UserCard";
import { goDo } from '../../server/funciones';





function UserAgregados() {
    const [users, setUsers] = useState([]);
    const [progress, setProgress] = useState(true);
    const [ofwhere, setOfWhere] = useState('');


    useEffect(() => {
        goDo.queryAllCollection('transferusers', (data,ofwhere) => {
            setProgress(false);
            setUsers(data);
            setOfWhere(ofwhere);
        })

    }, [])

    /**
     const user= (email,password)=>{
      try {
        const auth = getAuth();
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed in
          const user = userCredential.user;
          console.log(user);
          // ...
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
        });
      } catch (error) {
        
      }
    }
     */

    return (
        <>
{/* 
            <ResponsiveAppBar />
            <br />
            <br /><br /><br /> */}

            <Container>
                <Typography textAlign={'center'} style={{ color: '#717873' }} variant="h3" component="h1">
                    Usuarios Tranferidos

                </Typography>
                <br />
                {
          (ofwhere) ?
            <Alert>
              <AlertTitle>Data Traida</AlertTitle>
              La data se a traido de {ofwhere}
            </Alert>
            : void 0
        }
                <Grid container spacing={2}>



                    <UserCard noButtons={true} progress={progress} users={users} />


                </Grid>
            </Container>

        </>
    );
}

export default UserAgregados;

