
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import { red } from '@mui/material/colors';
import { DeleteForeverOutlined } from '@mui/icons-material';
import { Button, CircularProgress, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, Link, Alert } from '@mui/material';
import { useModalWithData } from '../../hooks/useModalWithData';
import AlertDialogSlide from '../../components/AlertDialogSlide/AlertDialogSlide'
import { useEffect, useState } from 'react';
import { useSnackbar } from 'notistack'
import { goDo } from '../../server/funciones';


export default function UserCard({ users, progress, noButtons }) {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [
    setIsModalOpened,
    isModalOpened,
    modalData,
    setModalData
  ] = useModalWithData()

  const [isprogress, setIsProgress] = useState(false)
  const [isRegisted, setRegisted] = useState(<></>)
  const [responseFirebase, setResponseFirebase] = useState({
    status: true,
    fuente: '',
    message: '',
    data: []
  })

  const isEmptyObject = (obj) => {
    for (var prop in obj) { if (obj.hasOwnProperty(prop)) return false; }
    return true;
  }

  const addUser = (e) => {
    if (!isEmptyObject(e)) {
      setIsProgress(true)
      let { name, usuario_id, email, sede_id } = e;
      let state = { email, name, sede_id, usuario_id }
      goDo.setDocument("transferusers/" + email,
        state, (status, fuente, message, data) => {
          setIsProgress(false)
          setResponseFirebase({ status, fuente, message, data })
        })
    }



  }



  const removeUser = (e) => {
    if (!isEmptyObject(e)) {
      goDo.deleteDocument('transferusers', e.email, (data) => {
        console.log(data);
      })
    }
  }

  useEffect(() => {
    let { status, fuente, message } = responseFirebase;
    if (status) {
      if (fuente === 'server') {
        enqueueSnackbar('Usuario Transferido Correctamente', {
          variant: 'success',
        });
      } else if (fuente === 'Local') {
        enqueueSnackbar('Usuario Transferido Correctamente modo Local', {
          variant: 'info',
        });
      }
    } else {
      enqueueSnackbar('Error: ' + message, {
        variant: 'error',
      });
    }
  }, [responseFirebase])


  return (
    <>

      {
        (progress) ?
          <CircularProgress style={{ color: 'red' }} />
          :
          <>

            <br />

            {
              users.map((e, i) => (
                <Grid key={i} item sm={6} xs={12} md={4}>
                  <Card key={i} sx={{ maxWidth: 345 }}>
                    <CardHeader
                      avatar={
                        <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                          {(e && e.name) ? (e.name.substr(0, 1)) : ""}
                        </Avatar>
                      }
                      action={
                        <>

                          {
                            (noButtons) ?
                              <IconButton onClick={() => removeUser(e)} aria-label="settings">
                                <DeleteForeverOutlined
                                />
                              </IconButton>
                              : void 0
                          }


                        </>
                      }
                      title={e.name}
                      subheader={e.email}
                    />

                    {
                      (!noButtons) ?
                        <Button
                          style={{ marginLeft: '10px', marginBottom: '10px' }}
                          onClick={() => {
                            setIsModalOpened(true)
                            setModalData(e)
                          }}
                          variant="outlined" color={'error'}>Transferir Usuario</Button>
                        : false
                    }

                  </Card>
                </Grid>

              ))
            }

          </>
      }
      <AlertDialogSlide
        isopen={isModalOpened}
        handleClose={() => {
          setIsModalOpened(false)
          setRegisted(false)
          setRegisted(false)
        }}
      >
        <DialogTitle>{'Tranferir Usuario'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Estas intentando transferir al usuario {(modalData) ? modalData.name : void 0}
            ¿Deseas transferirlo?
          </DialogContentText>
        </DialogContent>
        {
          (isprogress) ?
            <CircularProgress />
            :
            <>

              <DialogActions>
                {

                  <Button color="success" variant="contained" onClick={() => addUser(modalData)}>Aceptar</Button>

                }


                <Button onClick={() => {

                  setIsModalOpened(false)
                  setRegisted(false)
                  setRegisted(false)
                }}>Cerrar</Button>
              </DialogActions>
              {
                isRegisted
              }

            </>
        }
      </AlertDialogSlide>
    </>
  );
}
