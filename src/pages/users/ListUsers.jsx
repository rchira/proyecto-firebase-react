


import { useEffect, useState } from 'react';
import { Alert, AlertTitle, Container, Divider, Grid, Typography } from "@mui/material";
import UserCard from "./UserCard";
import ResponsiveAppBar from '../../components/nav/ResponsiveAppBar';
import { goDo } from '../../server/funciones';





function ListUsers() {
  const [users, setUsers] = useState([]);
  const [progress, setProgress] = useState(true);
  const [ofwhere, setOfWhere] = useState('');


  useEffect(() => {
    // SignIn('rchirasantos@gmail.com','R98A123',(message)=>{
    //   console.log(message);
    // })
    goDo.queryAllCollection('usuarios', (data, ofwhere) => {
   /*
    goDo.queryAllCollection('transferusers', (data2, ofwhere2) => {
 
        var setA = new Set(data),
          setB = new Set(data2),
         setC= setA.delete(setB);
       
        console.log(setC);
      })
   */

      setProgress(false);
      setUsers(data);
       setOfWhere(ofwhere);
      
    })

  }, [])

  return (
    <>

      {/* <ResponsiveAppBar />
      <br />
      <br /><br /><br /> */}


      <Container>
        <Typography textAlign={'center'} style={{ color: '#717873' }} variant="h4" component="h5">
          Usuarios Sin Transferir

        </Typography>

        {
          (ofwhere) ?
            <Alert>
              <AlertTitle>Data Traida</AlertTitle>
              La data se a traido de {ofwhere}
            </Alert>
            : void 0
        }


        <br />
        <Grid container spacing={2}>



          <UserCard progress={progress} users={users} />


        </Grid>
      </Container>

    </>
  );
}

export default ListUsers;

