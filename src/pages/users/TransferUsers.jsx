import { useEffect, useState } from "react"
import { useLocation, useParams } from 'react-router-dom'
import { Alert, CircularProgress } from "@mui/material";
import { goDo } from "../../server/firebase";

export function TransfersUser() {
    let [result, setResult] = useState(null)
    const [progress, setProgress] = useState(true);

    const {state} = useLocation()
       
    useEffect(() => {
       if(state.email && state.email.length>0){

            goDo.updateDocument("transferusers/" + state.email,
                state, (data) => {
                    setProgress(false)
                    setResult(data)
    
                })
        }
    })
    return (<>
        {
            (progress)?
            <CircularProgress style={{color:'red'}}/>
            :
            (result) ?
                <Alert severity="success">Usuario Registrado Correctamente</Alert>
                :
                <Alert severity="error">Error al registrar usuario</Alert>

        }
    </>)
}