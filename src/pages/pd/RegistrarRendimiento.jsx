import {
  Box,
  Button,
  Card,
  Grid,
  IconButton,
  TextField,
  Typography,
} from "@mui/material";
import BorderAllIcon from "@mui/icons-material/BorderAll";
import { useSnackbar } from "notistack";
import { useLocation, useNavigate } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useEffect, useState } from "react";
import ToolbarGrid from "../../components/DataGrid/DataGrid";
import metodos from "../../server/metodos";
import { columns } from "./Columnas/personalRendimientos_columnas";
import colocarScroll, {
  obtenerCantidadHoras,
  obtenerFechaActual,
} from "../../utils/helpers";
import { CAMPO } from "../../server/constantes/campos";
import AlertDialogSlide from "../../components/AlertDialogSlide/AlertDialogSlide";
import { useModalWithData } from "../../hooks/useModalWithData";
export function RegistrarRendimiento() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [setIsModalOpened, isModalOpened, modalData, setModalData] =
    useModalWithData();

  const { state } = useLocation();
  const navigate = useNavigate();
  const [datosLabor, setDatosLabor] = useState(
    state && state.labor ? state.labor : {}
  );
  const [parte_id, setParte_Id] = useState(
    state && state.parte_id ? state.parte_id : 0
  );
  const [numeroParteDiario, setNumeroParteDiario] = useState(
    state && state.numeroParteDiario ? state.numeroParteDiario : 0
  );
  const [lote, setLote] = useState(state && state.lote ? state.lote : 0);
  const [fundo, setFundo] = useState(state && state.fundo ? state.fundo : 0);
  const [elemento_pep_id, setElemento_pep_id] = useState(
    state && state.elemento_pep_id ? state.elemento_pep_id : 0
  );

  /**rendimientos de la labor establecida */
  const [rendimientoMinimo, setRendimientoMinimo] = useState(0);
  const [rendimientoMaximo, setRendimientoMaximo] = useState(0);

  /**dataset */
  const [dataPersonal, setDataPersonal] = useState([]);
  const [AsignacionRendimiento, setAsignacionRendimiento] = useState({
    rdto1: 0,
    rdto2: 0,
  });

  const [valoresporRegistroRendimiento, setValoresporRegistroRendimiento] =
    useState({});

  const registrarRendimientodelPersonal = async (
    personal_id,
    parametros = {}
  ) => {
    let { labor_id } = datosLabor || {};
    let resultado = await metodos.personal.registrarRendimientodelPersonal(
      parte_id,
      labor_id,
      personal_id,
      parametros
    );
    let { estadoregistrorendimiento } = resultado || {};

    if (estadoregistrorendimiento.status) {
      setIsModalOpened(false);
      enqueueSnackbar("Rendimiento Grabado", {
        variant: "success",
      });
      setAsignacionRendimiento({ rdto1: 0, rdto2: 0 });
    } else {
      enqueueSnackbar("Rendimiento no Grabado", {
        variant: "error",
      });
    }
  };

  const obtenerRendimientosdeLaLabor = async (elementopep) => {
    let data = await new Promise((resolve, reject) => {
      metodos.programaciones.listarProgramacionPorElementoPep(
        false,
        obtenerFechaActual(),
        elementopep,
        (data) => {
          resolve(data);
        }
      );
    });
    if (data.length > 0) {
      let {
        /*elemento_pep,
      elemento_pep_id,
      id,
      labor_id,
      lote_id,
      sede_id,*/
        rendimiento_maximo,
        rendimiento_minimo,
      } = data[0];
      setRendimientoMinimo(rendimiento_minimo);
      setRendimientoMaximo(rendimiento_maximo);

      obtenerPersonalregistradoenlaLabor(
        parte_id,
        datosLabor.labor_id,
        rendimiento_minimo,
        rendimiento_maximo
      );
    } else {
      obtenerPersonalregistradoenlaLabor(parte_id, datosLabor.labor_id, 0, 0);
    }
  };

  const obtenerPersonalregistradoenlaLabor = (
    parte_id,
    labor_id,
    rdto_minimo,
    rdto_maximo
  ) => {
    metodos.pd.listarPersonalPorParteDiario(
      true,
      parte_id,
      labor_id,
      (data) => {
        let lPersonal = data || [];
        let dataSet = lPersonal.map((e) => ({
          ...e,
          labor_id,
          parte_id,
          rdto_minimo,
          rdto_maximo,
        }));
        setDataPersonal(dataSet);
      }
    );
  };

  useEffect(() => {
    obtenerRendimientosdeLaLabor(elemento_pep_id);

    colocarScroll(0, 0, "smooth");
  }, []);

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={3} md={1}>
          <IconButton
            onClick={() => navigate(-1)}
            color="error"
            aria-label="add to shopping cart"
          >
            <ArrowBackIcon />
          </IconButton>
        </Grid>
        <Grid item xs={9} md={11}>
          <Typography variant="h6" component="h6">
            N° {numeroParteDiario}
            <Box sx={{ ml: 3 }} component={"span"}>
              {lote} -{" "}
            </Box>
            <Box component={"span"}>{fundo}</Box>
            <Box component={"div"} fontSize={15} sx={{ ml: 2 }}>
              {datosLabor.labor}
            </Box>
          </Typography>
          <br />
        </Grid>
      </Grid>
      <Card sx={{ p: 2 }}>
        <Grid container spacing={2}>
          <Grid item xs={6} md={3}>
            <TextField
              required
              id="outlined-required"
              label="Minimo"
              value={rendimientoMinimo}
              InputProps={{
                readOnly: true,
              }}
            />
          </Grid>

          <Grid item xs={6} md={3}>
            <TextField
              required
              id="outlined-required"
              label="Maximo"
              value={rendimientoMaximo}
              InputProps={{
                readOnly: true,
              }}
            />
          </Grid>
        </Grid>
      </Card>

      <Grid item xs={12} md={12}>
        <Typography
          sx={{
            display: "flex",
            marginTop: 5,
            alignItems: "center",
          }}
          variant="h6"
          component="h6"
        >
          <BorderAllIcon sx={{ mr: 1 }} /> Registro de rendimientos
        </Typography>
      </Grid>
      <Grid item xs={12} md={12}>
        <ToolbarGrid
          keyRow={"id"}
          configColumns={columns}
          data={dataPersonal}
          onCellEditCommit={(e) => {
            /**
            * 
             console.log(e);
            
            let { id, value } = e || {};
            setIsModalOpened(true);
            */
          }}
          onCellClick={(e) => {
            let { rendimiento, rendimiento2 } = e.row || {};
            setAsignacionRendimiento({
              rdto1: rendimiento,
              rdto2: rendimiento2,
            });
            setModalData(e.row);
            setIsModalOpened(true);
          }}
        />
      </Grid>
      {modalData ? (
        <AlertDialogSlide
          isopen={isModalOpened}
          handleClose={() => {
            //  cuando queremos cerrar
            //  por si queremos validar
          }}
          cerrarModalMedianteBoton={() => {
            setIsModalOpened(false);
          }}
        >
          <Typography
            sx={{
              display: "flex",
              marginTop: 5,
              alignItems: "center",
            }}
            variant="h6"
            component="h6"
          >
            Ingreso de Rendimiento
          </Typography>
          <Grid sx={{ mt: 5 }} container spacing={2}>
            <Grid item xs={12} md={12}>
              <Box sx={{ mr: 2 }} component={"strong"}>
                Nombre:{" "}
              </Box>
              <Box component={"label"}>{modalData.personal}</Box>
            </Grid>
            <Grid item xs={12} md={12}>
              <Box sx={{ mr: 2 }} component={"strong"}>
                DNI:{" "}
              </Box>
              <Box component={"label"}>{modalData.numero_documento}</Box>
            </Grid>
            <Grid item xs={12} md={12}>
              <Box sx={{ mr: 2 }} component={"strong"}>
                Horas T:{" "}
              </Box>
              <Box component={"label"}>
                {
                  //
                  obtenerCantidadHoras(modalData.inicio)
                }
              </Box>
            </Grid>

            <Grid item xs={6} md={6}>
              <TextField
                type={"number"}
                required
                id="outlined-required"
                value={AsignacionRendimiento.rdto1}
                label={datosLabor.um_rendimiento_1}
                onChange={(e) => {
                  setAsignacionRendimiento({
                    ...AsignacionRendimiento,
                    rdto1: e.target.value,
                  });
                }}
              />
            </Grid>
            <Grid item xs={6} md={6}>
              <TextField
                type={"number"}
                required
                value={AsignacionRendimiento.rdto2}
                onChange={(e) => {
                  setAsignacionRendimiento({
                    ...AsignacionRendimiento,
                    rdto2: e.target.value,
                  });
                }}
                id="outlined-required"
                label={datosLabor.um_rendimiento_2}
              />
            </Grid>
            <Grid item xs={6} md={12}>
              <Button
                onClick={() => {
                  registrarRendimientodelPersonal(modalData.id, {
                    rendimiento: AsignacionRendimiento.rdto1 || 0,
                    rendimiento2: AsignacionRendimiento.rdto2 || 0,
                  });
                }}
                size="large"
                variant="contained"
                color="error"
              >
                Grabar
              </Button>
            </Grid>
          </Grid>
        </AlertDialogSlide>
      ) : (
        void 0
      )}
    </>
  );
}
