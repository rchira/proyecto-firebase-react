import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  Typography,
  Alert,
  Tab,
} from "@mui/material";
import { useEffect, useState } from "react";
import Asynchronous from "../../components/Autocomplete";
import MaterialUIPickers from "../../components/Date/date";
import { format } from "date-fns";
import { removeDuplicates } from "../../utils/helpers";
import AddTaskIcon from "@mui/icons-material/AddTask";
import ToolbarGrid from "../../components/DataGrid/DataGrid";
import { columns } from "./Columnas/personal_columnas";
import { useNavigate } from "react-router-dom";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import { Chart } from "../../components/Chart/Line";
import { goDo } from "../../server/funciones";



export function ListPersonalPD() {
  

  const [valueDate, setValueDate] = useState(new Date());
  const [idParteDiario, setIdParteDiario] = useState("");
  const [idLabor, setIdLabor] = useState(0);

  const [dataParteDiarios, setDataParteDiarios] = useState([]);
  const [dataLabores, setDataLabores] = useState([]);
  const [dataPersonal, setDataPersonal] = useState([]);
  const [infoParteDiario, setinfoParteDiario] = useState([]);
  const [rendimientoLabor, setRendimientoLabor] = useState({});
  const [labels, setlabel] = useState([]);
  const [dataset, setDataSet] = useState([]);

  const [valueTab, setValueTab] = useState("1");

  const handleChangeTab = (event, newValue) => {
    setValueTab(newValue);
  };

  const handleChange = (newValue) => {
    setValueDate(newValue);
  };
  const renderDataInChar = () => {
    let personal = dataPersonal.map((e) => e.personal) || [];
    let rendimiento = dataPersonal.map((e) => e.rendimiento) || [];
    let rendimiento2 = dataPersonal.map((e) => e.rendimiento_2) || [];
    let data_set = [
      {
        label: "Rendimiento 1",
        data: rendimiento,
        borderColor: "rgb(255, 99, 132)",
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
      {
        label: "Rendimiento 2",
        data: rendimiento2,
        borderColor: "rgb(53, 162, 235)",
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
    ];
    setlabel(personal);
    setDataSet(data_set);
  };
  const getRendimientoLabor = (elemento_pep_id) => {
    let fech = format(new Date(valueDate), "yyyy-MM-dd");
console.log(fech+'  y el elemento pep es :'+elemento_pep_id);
    goDo.queryCollectionByParams(
      `programaciones/${fech}/elementos_peps`,
      { column: "elemento_pep", operator: "==", value: elemento_pep_id },
      (data, ofwhere) => {
          console.log(data);
        if (data && data.length > 0) {
            console.log(data[0]);
          setRendimientoLabor(data[0]);
        }
      }
    );
  };

  const getLaboresParteDiarios = (numero) => {
    goDo.queryAllCollection(
      "parte_diario/" + numero + "/labores",
      (data, ofwhere) => {
        setDataLabores(data);
      }
    );
  };

  const getPartesDiarios = (date) => {
    goDo.queryCollectionByParams(
      "parte_diario",
      { column: "fecha", operator: "==", value: date },
      (data, ofwhere) => {
        // console.log(removeDuplicates(data, "numero")[0]);
        setDataParteDiarios(removeDuplicates(data, "numero"));
      }
    );
  };
  const getListPersonal = () => {
    if (idParteDiario && idLabor > 0) {
      setDataPersonal([]);
      goDo.queryCollectionByParams(
        "parte_diario/" + idParteDiario + "/labores/" + idLabor + "/personal",
        { column: "esActivo", operator: "==", value: true },
        (data, ofwhere) => {
          console.log(data);
          setDataPersonal(data);
        }
      );
    }
  };
  useEffect(() => {
    let fech = format(new Date(valueDate), "yyyy-MM-dd");
    if (fech) {
      getPartesDiarios(fech);
      setIdParteDiario(0);
      setDataParteDiarios([]);
      setIdLabor(0);
      setDataLabores([]);
      setDataPersonal([]);
    }
  }, [valueDate]);

  useEffect(() => {
    if (idParteDiario) {
      getLaboresParteDiarios(idParteDiario);
      setIdLabor(0);
      setDataLabores([]);
      setDataPersonal([]);
    }
  }, [idParteDiario]);
  useEffect(() => {
    if (dataPersonal.length > 0) {
      renderDataInChar();
    }
  }, [dataPersonal]);

  return (
    <>
      <FormControl sx={{ flexGrow: 1, width: "100%" }}>
        <Typography
          textAlign={"left"}
          style={{ color: "#717873" }}
          variant="h5"
          component="h5"
        >
          <AddTaskIcon /> Consulta de personal en Parte Diario
        </Typography>
        <br />
        <Grid container spacing={2}>
          <Grid item xs={12} md={3}>
            <MaterialUIPickers
              label="Selecciona Fecha"
              value={valueDate}
              handleChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            {/* <Item> */}

            <Asynchronous
              label={`(${dataParteDiarios.length}) Partes diarios`}
              Namelabel={"numero"}
              IdLabel={"id"}
              getSelected={(item, object) => {
                setIdParteDiario(item.id);
                setinfoParteDiario(object);
              }}
              valueInput={"1644923813"}
              data={dataParteDiarios}
            />

            {/* </Item> */}
          </Grid>

          <Grid item xs={12} md={3}>
            {/* <Item> */}

            <Asynchronous
              label={`(${dataLabores.length}) Labores`}
              Namelabel={"labor"}
              IdLabel={"labor_id"}
              getSelected={(item, object) => {
                setIdLabor(item.labor_id);
                console.log(object);
                getRendimientoLabor(object.elemento_pep_id);
              }}
              
              data={dataLabores}
            />

            {/* </Item> */}
          </Grid>
          <Grid item xs={12} md={3}>
            <Button
              onClick={getListPersonal}
              variant="contained"
              color="success"
            >
              Consultar
            </Button>
          </Grid>

          {idParteDiario ? (
            <>
              <Grid item  >
                <strong style={{ marginRight: "12px" }}>Registrado Por:</strong>
                <label>{infoParteDiario.usuario_creacion}</label>
              </Grid>
              <Grid  item >
                <strong>Sede:</strong>
                <label>
                  {infoParteDiario.sede_id == "1200" ? "Piura" : "Ica"}
                </label>
              </Grid>
              <Grid item >
                <strong>Lote:</strong>
                <label>{infoParteDiario.lote}</label>
              </Grid>
              <Grid item >
                <strong>Fundo:</strong>
                <label>{infoParteDiario.fundo}</label>
              </Grid>
              <Grid item >
                <strong>Activo:</strong>

                <label>{infoParteDiario.esActivo ? "SI" : "NO"}</label>
              </Grid>
              <Grid item >
                <strong>Cerrado:</strong>
                <label>{infoParteDiario.estacerrado ? "SI" : "NO"}</label>
              </Grid>
            </>
          ) : (
            void 0
          )}

          <Grid item xs={12} md={12}>
            <Divider />
          </Grid>
          <Grid item xs={12} md={12}>
            <TabContext value={valueTab}>
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <TabList
                  onChange={handleChangeTab}
                  aria-label="lab API tabs example"
                >
                  <Tab label="Lista" value="1" />
                  <Tab label="Cuadro Estadistico" value="2" />
                </TabList>
              </Box>
              <TabPanel style={{ padding: 0 }} value="1">
                <ToolbarGrid
                  /**
                                      onRowDoubleClick={(e) => {
                                         let dP = e.row || []
                                         navigate('/app/detallerendimientopersonal',
                                             {
                                                 state: {
                                                     partediarioId: idParteDiario,
                                                     idLabor: idLabor,
                                                     fechaParteDiario: valueDate,
                                                     datapesl: dP
                                                 }
                                             })
                                     }}
                                     */
                  keyRow={"id"}
                  configColumns={columns}
                  data={dataPersonal}
                />
              </TabPanel>
              <TabPanel style={{ padding: 0 }} value="2">
                <Grid container spacing={2}>
                    {
                        console.log(rendimientoLabor)
                    }
                  <Grid item xs={12} md={4}>
                    <strong style={{ marginRight: 10 }}>Elemento Pep </strong>
                    <label>{rendimientoLabor.elemento_pep}</label>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <strong style={{ marginRight: 10 }}>
                      Rendimiento Minimo{" "}
                    </strong>
                    <label>{rendimientoLabor.rendimiento_minimo}</label>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <strong style={{ marginRight: 10 }}>
                      Rendimiento Maximo{" "}
                    </strong>
                    <label>{rendimientoLabor.rendimiento_maximo}</label>
                  </Grid>
                </Grid>
                <Chart labels={labels} dataset={dataset} />
              </TabPanel>
            </TabContext>
          </Grid>
        </Grid>
      </FormControl>
    </>
  );
}
