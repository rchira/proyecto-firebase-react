import { Fragment, useEffect, useState } from "react";
import {
  Avatar,
  Badge,
  Box,
  Button,
  Card,
  Chip,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import GroupAddIcon from "@mui/icons-material/GroupAdd";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { margin } from "@mui/system";
import metodos from "../../../server/metodos";
import { EstadoRegistro } from "../../../components/EstadoRegistro";
import { useNavigate } from "react-router-dom";
import { PATH } from "../../../utils/path";
import { TieneRendimientos } from "./TieneRendimientos";
import DirectionsOffIcon from '@mui/icons-material/DirectionsOff';
import { TooltipCustom } from "../../../components/TooltipCustom";

const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: "0 4px",
  },
}));

export function ListLabores({
  lote,
  fundo,
  parte_id,
  numero,
  expanded,
  estaCerrado,
  omitirLabor,
}) {
  let navigate = useNavigate();
  const [labores, setLabores] = useState([]);

  const obtenerLaboresPorParteDiario = (parteid) => {
    metodos.pd.listarLaboresPorParteDiario(true, parteid, (data) => {
      setLabores(data);
    });
  };

  useEffect(() => {
    if (parte_id) {
      obtenerLaboresPorParteDiario(parte_id);
    }
  }, []);
  return (
    <>
      <List
        sx={{
          width: "100%",
          maxWidth: "100%",
          bgcolor: "background.paper",
        }}
        subheader={<ListSubheader><Chip size={'small'} avatar={<Avatar>{labores.length}</Avatar>} label="Labores" /> </ListSubheader>}
      >
        {labores.map((ee, ii) => (
          <Fragment key={ee.labor_id}>
            <ListItem
              sx={{
                opacity: !ee.esOmitido ? 1 : 0.3,
              }}
              key={ee.labor_id}
            >
              {/* <ListItemIcon>
                      <WifiIcon />
                    </ListItemIcon> */}
              {/* <ListItemText  primary={ee.labor_id}/> */}
              <EstadoRegistro
                sx={{ mr: 2 }}
                fecha={ee.fecha_creacion_labor_firestore}
              />
              <ListItemText
                sx={{
                  wordWrap: "",
                }}
                primary={`${ee.labor_id} ${ee.labor} `}
                secondary={ee.elemento_pep_id}
              />

              {false ? (
                <IconButton sx={{ mr: 2 }} aria-label="cart">
                  <StyledBadge badgeContent={4} color="error">
                    <VisibilityIcon />
                  </StyledBadge>
                </IconButton>
              ) : (
                void 0
              )}

              {!ee.esOmitido && !estaCerrado ? (
                <TooltipCustom 
                title={'Anular Labor'}
                >

<Button
                  onClick={() => {
                    omitirLabor(parte_id, ee.labor_id);
                  }}
                  size="small"
                  color="error"
                  sx={{ mr: 1, float: "right" }}
                  endIcon={<DirectionsOffIcon />}
                >
                  {/* Omitir Labor */}
                </Button>
                </TooltipCustom>
              ) : (
                void 0
              )}

              <IconButton
                color={!ee.esOmitido ? "error" : "inherit"}
                onClick={() => {
                  if (!ee.esOmitido) {
                    navigate("/app/asistencia", {
                      state: {
                        parte_id: parte_id,
                        labor_id: ee.labor_id,
                        labor: ee.labor,
                        numero_parte: numero,
                        principal: ee.principal,
                        fundo,
                        lote,
                        expanded,
                        estaCerrado
                      },
                    });
                  }
                }}
                aria-label="cart"
              >
                <StyledBadge
                  badgeContent={ee.personal_total || 0}
                  color="error"
                >
                  <GroupAddIcon />
                </StyledBadge>
              </IconButton>
            </ListItem>
            {ee.principal ? (
              <>
                <TieneRendimientos
                  lote={lote}
                  elemento_pep_id={ee.elemento_pep_id}
                  fundo={fundo}
                  numero={numero}
                  parte_id={parte_id}
                  labor_id={ee.labor_id}
                  expanded
                />
                <Divider />
              </>
            ) : (
              void 0
            )}
          </Fragment>
        ))}
      </List>
    </>
  );
}
