import { Button } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import metodos from "../../../server/metodos";
import { PATH } from "../../../utils/path";
import EmojiEventsOutlinedIcon from '@mui/icons-material/EmojiEventsOutlined';
export function TieneRendimientos({lote,elemento_pep_id,fundo,numero,parte_id,labor_id}){
    let navigate = useNavigate();
    const [datosdelLabor, setdatosdelLabor] = useState({});

    const obtenerdatosdelaLabor = () => {
      metodos.labores.obtenerLaborPorId(false, labor_id, (data) => {
        setdatosdelLabor(data.data);
      });
    };
    useEffect(() => {
      if (labor_id) {
        obtenerdatosdelaLabor();
      }
    }, []);

    return (<>
    
    {
        (datosdelLabor.tiene_rendimiento_1=="1" || datosdelLabor.tiene_rendimiento_2=="1")?
        <Button  onClick={()=>{
            
            navigate(PATH.REGISTRARRENDIMIENTO, {
             state: {
               parte_id: parte_id,
               labor: datosdelLabor,
               numeroParteDiario: numero,
               lote,fundo,
               elemento_pep_id
             },
           });
          }} sx={{ml:7,mb:3,borderRadius:25}} size="small" variant="contained" color="error">
            <EmojiEventsOutlinedIcon/>Rendimientos</Button>

        :  void 0
    }
   
    
    </>)
}