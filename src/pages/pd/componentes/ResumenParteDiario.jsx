import { Card, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { useState, useEffect } from "react";
import ToolbarGrid from "../../../components/DataGrid/DataGrid";
import metodos from "../../../server/metodos";
import { columns } from "../Columnas/resumen_pd_columnas";

export function ResumenParteDiario({
  parte_id,
  numero_pd,
  lote,
  fundo,
  usuario,
}) {
  const [dataResumenParteDiario, setDataResumenParteDiario] = useState([]);
  const obtenerResumen  =async () => {
    if (parte_id) {
      console.log(parte_id);
      const data = await metodos.pd.obtenerResumendelParteDiarioModel(parte_id);
      console.log(data);
      setDataResumenParteDiario(data);
    }
  };
  useEffect(() => {
    obtenerResumen();
  }, []);
  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} md={12}>
          <center>
            <Typography variant="h6" component="h6">
              N° {numero_pd}
              <Box sx={{ ml: 3 }} component={"span"}>
                {lote} -{" "}
              </Box>
              <Box component={"span"}>{fundo}</Box>
              {/* <Box component={"div"} fontSize={15} sx={{  }}>
              {labor}
            </Box> */}
            </Typography>
          </center>
          <Grid item xs={12} md={12}>
            <Typography variant="h6" component="h6">
              <center> {usuario}</center>
            </Typography>
          </Grid>
          <br />
        </Grid>

        <ToolbarGrid
          keyRow={"labor_id"}
          configColumns={columns}
          data={dataResumenParteDiario}
        />
      </Grid>
    </>
  );
}
