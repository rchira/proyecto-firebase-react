import {
  Button,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import LegendToggleIcon from "@mui/icons-material/LegendToggle";
import { ResumenParteDiario } from "./ResumenParteDiario";
import { ConfirmDialog } from "../../../components/ConfirmDialog";
export function OpcionesResumen({ data, verResumenParteDiarioHandle,eliminarparteDiario }) {
  let { numero_pd, lote, fundo, usuario,parte_id } = data || {};
  return (
    <>
      <DialogTitle>{`N° ${numero_pd}`}</DialogTitle>
      <DialogContent>
        <DialogContentText >
          <Button
            onClick={() => {
              verResumenParteDiarioHandle(
                <ResumenParteDiario
                  numero_pd={numero_pd}
                  lote={lote}
                  fundo={fundo}
                  usuario={usuario}
                  parte_id={parte_id}
                />
              );
            }}
            endIcon={<LegendToggleIcon />}
            size="large"
            sx={{ mb: 5 }}
            variant="contained"
            color="error"
          >
            Resumen Parte Diario
          </Button>
          <br />
          <Button
          onClick={()=>{
            ConfirmDialog(`¿Estas seguro que deseas eliminar el parte diario?`, () => {
              eliminarparteDiario(parte_id)
            })                
          }}
            endIcon={<DeleteIcon />}
            size="large"
            variant="contained"
            color="inherit"
          >
            Eliminar Este Parte Diario
          </Button>
        </DialogContentText>
      </DialogContent>
    </>
  );
}
