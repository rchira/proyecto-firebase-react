import { Button } from "@mui/material";
import { format } from "date-fns";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import { GridActionsCellItem } from "@mui/x-data-grid";
import { eliminarPersonalPorParteDiario } from "../../../server/metodos/pd";
import { EstadoRegistro } from "../../../components/EstadoRegistro"
import { ConfirmDialog } from "../../../components/ConfirmDialog"
const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', };
export const columns = [
  {
    field: "actions",
    type: "actions",
    getActions: (params) => [
      <GridActionsCellItem
        icon={<DeleteOutlineOutlinedIcon />}
        onClick={() => {
          if(!params.row.estaCerrado){
            ConfirmDialog(`¿Estas seguro que deseas eliminarlo?`, () => {
              let { parteId, laborId, id } = params.row || {};
              eliminarPersonalPorParteDiario(parteId, laborId, id);
            });
           
          }
         
        }}
        label="Delete"
      />,
      //   <GridActionsCellItem icon={...} onClick={...} label="Print" showInMenu />,
    ],
  },
  {
    field: "estado",
    headerName: "Estado",
    width: 75,
    type: "",
    renderCell: (f) => {
      let { fecha_registro_asistencia } = f.row || {};
      return <EstadoRegistro fecha={fecha_registro_asistencia} />;
    },
  },
  {
    field: "numero_documento",
    headerName: "DNI ",
    width: 150,
  },
  {
    field: "personal",
    headerName: "Apellidos y Nombres",
    width: 250,
  },

  {
    field: "horas",
    headerName: "HORAS ",
    width: 80,
    valueGetter: (f) => {
      let { inicio } = f.row || {};
      let fechaGuardada = inicio.toDate();
      let fechaActual = new Date();
      let diferencia = Math.abs(fechaActual - fechaGuardada);
      let horas = parseInt(diferencia / (1000 * 3600));
      return horas;
    },
  },
  {
    field: "inicio",
    headerName: "Fech. Registro",
    width: 450,
    type: "datetime",
    valueGetter: ({ value }) => value.toDate().toLocaleTimeString("es-ES", options),
  },
];
