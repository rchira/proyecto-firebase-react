
import VisibilityIcon from '@mui/icons-material/Visibility';
import { format } from 'date-fns';
export const columns = [

  {
    field: 'personal_id',
    headerName: 'PERSONAL ID',
    width: 150,

  },
  {
    field: 'personal',
    headerName: 'Personal ',
    width: 320,

  },

  {
    field: 'numero_documento',
    headerName: 'DNI ',
    width: 80,

  },
  {
    field: 'rendimiento',
    headerName: 'RDMT. 1 ',
    width: 100,

  }
  ,
  {
    field: 'rendimiento_2',
    headerName: 'RDMT. 2',
    width: 100,

  },
  {
    field: 'fecha_registro',
    headerName: 'FECHA REGISTRO ',
    width: 250,

  },
  {
    field: 'fecha_registro_asistencia',
    headerName: 'FECH. REGISTRO ASISTENCIA ',
    width: 200,
    valueGetter: (params) => {
     return `${params.value}`
    }
  },
  {
    field: 'esActivo',
    headerName: 'Activo ',
    width: 80,

  },
  {
    field: 'esExterno',
    headerName: 'Externo ',
    width: 80,

  },
  {
    field: 'esPrincipal',
    headerName: 'Principal ',
    width: 80,

  },
  {
    field: 'tipo_salida',
    headerName: 'Tipo Salida ',
    width: 200,

  }



  /**
     {
    field: 'sede',
    headerName: 'SEDE',
     description: '',
    sortable: false,
       width: 160,
       valueGetter: (params) =>
       `${params.row.sede_id=='1100'?'Ica': 'Piura'}`,
    },
   */
];
