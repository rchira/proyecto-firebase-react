import { Button, Chip, TextField } from "@mui/material";
import { format } from "date-fns";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import { GridActionsCellItem } from "@mui/x-data-grid";
import { eliminarPersonalPorParteDiario } from "../../../server/metodos/pd";
import { EstadoRegistro } from "../../../components/EstadoRegistro";
import { estaOpcionExisteEnElObjeto } from "../../../utils/helpers";
const options = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric",
};

export const columns = [
  /*
  
  {
    field: "actions",
    type: "actions",
    getActions: (params) => [
      <GridActionsCellItem
        icon={<DeleteOutlineOutlinedIcon />}
        onClick={() => {
          let confirm = window.confirm("¿Estas seguro que deseas eliminarlo?");
          if (confirm) {
            let { parteId, laborId, id } = params.row || {};
            eliminarPersonalPorParteDiario(parteId, laborId, id);
          }
        }}
        label="Delete"
      />,
      //   <GridActionsCellItem icon={...} onClick={...} label="Print" showInMenu />,
    ],
  },
  */
 {
   field: "fecha_registro_rendimiento_firestore",
   headerName: "Estado",
   width: 75,
   type: "",
   renderCell: (f) => {
     let { fecha_registro_rendimiento_firestore } = f.row || {};
     if(fecha_registro_rendimiento_firestore!==undefined){
       return <EstadoRegistro fecha={fecha_registro_rendimiento_firestore} />;
      }else{
        return <div></div>
      }
    },
  },
  {
    field: "estadorendimiento",
    headerName: "E.R.D.P",
    width: 150,
    type: "",
    renderCell: (f) => {
      let { rdto_minimo, rdto_maximo, rendimiento, rendimiento2 } =
        f.row || {};
      let promedio = ((parseInt(rendimiento) + parseInt(rendimiento2)) / 2);
      // alert(promedio)
      let status = ""
      if (promedio < rdto_minimo) {
        status = <Chip  color="error" label="No alcanzado" size="small" variant="filled" />
      } else if (
        promedio >= rdto_minimo &&
        promedio <= rdto_maximo
      ) {
        status = <Chip color="success" label="Alcanzado" size="small" variant="filled" />
      } else if (promedio > rdto_maximo) {
        status = <Chip color="info" label="Superado" size="small" variant="filled"  />;
      }
      return <div>{status}</div>;
    },
  },

  {
    field: "numero_documento",
    headerName: "DNI ",
    width: 150,
  },
  {
    field: "personal",
    headerName: "Apellidos y Nombres",
    width: 250,
  },

  {
    field: "horas",
    headerName: "H.T",
    width: 80,
    valueGetter: (f) => {
      let { inicio } = f.row || {};
      let fechaGuardada = inicio.toDate();
      let fechaActual = new Date();
      let diferencia = Math.abs(fechaActual - fechaGuardada);
      let horas = parseInt(diferencia / (1000 * 3600));
      return horas;
    },
  },
  {
    field: "rendimiento",
    headerName: "Rdto",
    width: 100,
    type: "number",
    editable: false,
  },
  {
    field: "rendimiento2",
    headerName: "Rdto2",
    width: 100,
    type: "number",
    editable: false,
  },

  /*
  {
    field: "inicio",
    headerName: "Fech. Registro",
    width: 450,
    type: "datetime",
    valueGetter: ({ value }) => value.toDate().toLocaleTimeString("es-ES", options),
  }
  */
];
