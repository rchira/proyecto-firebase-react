

export const columns = [

  {
    field: 'labor',
    headerName: 'LABOR',
    width: 190,

  },
  {
    field: 'personal_total',
    headerName: 'N° DE PERSONAS ',
    width: 60,
    valueGetter: (params) => {
      return `${params.value || 0}`
     }

  },

 /**
   {
    field: '',
    headerName: 'Jhu ',
    width: 60,

  },
   */
  {
    field: 'rendimiento_total',
    headerName: 'Rdto.1',
    width: 70,
    valueGetter: (params) => {
      return `${params.value || 0}`
     }

  }
  ,
  {
    field: 'rendimiento_total_2',
    headerName: 'Rdto.2',
    width: 70,
    valueGetter: (params) => {
      return `${params.value || 0}`
     }

  }
 
];
