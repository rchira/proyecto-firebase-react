import { Send } from "@mui/icons-material";
import AddTask from "@mui/icons-material/AddTask";
import {
  Alert,
  Button,
  Checkbox,
  Divider,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import Asynchronous from "../../components/Autocomplete";
import InputWithLector from "../../components/InputWithLector";
import {
  aleatorio,
  removeDuplicates,
  getUser,
  getIdAutogenerado,
  obtenerUbicacion,
} from "../../utils/helpers";
import { useSnackbar } from "notistack";
import { goDo, haveDo } from "../../server/funciones";
import metodos from "../../server/metodos";
import { Footer } from "../../components/Footer";
import { CAMPO } from "../../server/constantes/campos";
import { format } from "date-fns";
import { CompBackdrop } from "../../components/Backdrop";
import { SENTENCIAS } from "../../utils/sentencias";

export default function Crearpd() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  /**varibales momentanias */
  const [sedeId, setSedeId] = useState(0);
  const [usuarioId, setUsuarioId] = useState(0);
  const [usuarioName, setUsuarioName] = useState("");

  /**variables dianmicas */
  const [tipoProyectoId, setTipoProyectoId] = useState("");
  const [cultipoId, setCultivoId] = useState("");
  const [companiaId, setCompaniaId] = useState("");
  const [fundoId, setFundoId] = useState("");
  const [lote, setLote] = useState("");
  const [loteId, setLoteId] = useState("");
  const [LaborId, setLaborId] = useState(0);
  const [FaseId, setFaseId] = useState("");
  const [procesoId, setProcesoId] = useState("");
  const [numeroRepaso, setnumeroRepaso] = useState(0);
  const [turno, setTurno] = useState("M");
  const [localizacion, setLocalizacion] = useState([]);

  /**variables de data */
  const [dataCultivo, setDataCultivo] = useState([]);
  const [dataCompania, setDataCompania] = useState([]);
  const [dataFundo, setDataFundo] = useState([]);
  const [dataLote, setDataLote] = useState([]);
  const [dataLabores, setDataLabores] = useState([]);
  const [dataFase, setDataFase] = useState([]);
  const [dataProceso, setDataProceso] = useState([]);

  /**variables alertas */
  const [responseFirebase, setResponseFirebase] = useState({
    status: true,
    fuente: "",
    message: "",
    id: "",
  });

  const [openBackdrop, setOpenBackdrop] = useState(false);
  const [mensajeBackdrop, setMensajeBackdrop] = useState("");

  const getCultivo = (tipoProyecto_Id) => {
    metodos.maestros.obtenerCultivos(false,
      sedeId,
      tipoProyecto_Id,
      (data, whereIs) => {
        if (data && data.length !== dataCultivo.length) {
          setDataCultivo(data);
        }
      }
    );
  };
  const getCompania = (tipoProyecto_Id, cultipoId) => {
    metodos.maestros.obtenerCompanias(false,
      sedeId,
      tipoProyecto_Id,
      cultipoId,
      (data, where) => {
        setDataCompania(data);
      }
    );
  };

  const getFundo = (tipoProyecto_Id, cultipoId, companiaId) => {
    metodos.maestros.obtenerFundos(
      false,
      sedeId,
      tipoProyecto_Id,
      cultipoId,
      companiaId,
      (data, where) => {
        setDataFundo(data);
      }
    );
  };

  const getLote = (tipoProyecto_Id, cultipoId, companiaId, fundoId) => {
    metodos.maestros.obtenerLotes(
      false,
      sedeId,
      tipoProyecto_Id,
      cultipoId,
      companiaId,
      fundoId,
      (data, where) => {
        setDataLote(data);
      }
    );
  };

  const getLabores = () => {
    metodos.labores.obtenerLabores(false,(data, where) => {
      setDataLabores(data);
    });
  };

  const getFase = () => {
    metodos.fases.obtenerFases(false,sedeId, (data, where) => {
      setDataFase(data);
    });
  };

  const getProceso = (fase_id) => {
    metodos.fases.obtenerFasesProcesosPorFaseId(false,sedeId, fase_id, (data, where) => {
      setDataProceso(data);
    });
  };

  const crearParteDiario = async () => {
    if (localizacion.length > 0) {
      if (
        (tipoProyectoId == "P" || tipoProyectoId == "I") &&
        cultipoId !== "" &&
        companiaId !== "" &&
        fundoId !== "" &&
        loteId !== "" &&
        FaseId !== "" &&
        procesoId !== "" &&
        LaborId > 0 &&
        numeroRepaso >= 0
      ) {
        setOpenBackdrop(true);
        setMensajeBackdrop("Creando Parte Diario");

        const fecha = format(new Date(), "yyyy-MM-dd");
        let { estado, resultado } = await metodos.pd.crearParteDiarioModel({
          [CAMPO.COMPANIAID]: companiaId,
          [CAMPO.CREATEDONVERSIONAPP]: "Web",
          [CAMPO.ESACTIVO]: true,
          [CAMPO.ESTACERRADO]: false,
          [CAMPO.ESTADESCARGADO]: false,
          [CAMPO.FASEID]: FaseId,
          [CAMPO.FECHA]: fecha,
          [CAMPO.FECHACREACION]: haveDo.Timestamp.fromDate(new Date()),
          [CAMPO.FECHACREACIONPARTEDIARIOFIRESTORE]: haveDo.serverTimestamp(),
          [CAMPO.FUNDO]: fundoId,
          [CAMPO.LABORID]: LaborId,
          [CAMPO.LOTE]: lote,
          [CAMPO.LOTEID]: loteId,
          [CAMPO.NUMEROREPASO]: numeroRepaso,
          [CAMPO.POSICIONPARTEDIARIO]: localizacion,
          [CAMPO.PROCESOID]: procesoId,
          [CAMPO.SEDEID]: sedeId,
          [CAMPO.TIPOPROYECTOID]: tipoProyectoId,
          [CAMPO.TURNO]: turno,
          [CAMPO.USUARIOCREACION]: usuarioName,
          [CAMPO.USUARIOCREACIONID]: usuarioId,
        });
        setOpenBackdrop(false);
        setMensajeBackdrop("");

        if (estado == "resolved") {
          let { historialRegistroCabeceraPD } = resultado;
          if (historialRegistroCabeceraPD.status) {
            enqueueSnackbar("Parte Diario Creado", {
              variant: "success",
            });
            limpiarElementosTodos();
          } else {
            enqueueSnackbar("No se pudo crear Parte Diario ", {
              variant: "error",
            });
          }
        } else if (estado == "error") {
          enqueueSnackbar(
            "Error catastrofico al procesar la transacción de crear Parte Diario",
            {
              variant: "error",
            }
          );
        }
      } else {
        enqueueSnackbar(SENTENCIAS[20], {
          variant: "error",
        });
      }
    } else {
      enqueueSnackbar(SENTENCIAS[19], {
        variant: "warning",
      });
    }
  };

  useEffect(() => {
    getCultivo(tipoProyectoId);
  }, [tipoProyectoId]);

  useEffect(() => {
    getCompania(tipoProyectoId, cultipoId);
  }, [cultipoId]);

  useEffect(() => {
    /**listamos los fundos */
    getFundo(tipoProyectoId, cultipoId, companiaId);
  }, [companiaId]);

  useEffect(() => {
    /**listamos los lotes */
    getLote(tipoProyectoId, cultipoId, companiaId, fundoId);
  }, [fundoId]);

  useEffect(() => {
    /**listamos las fases */
    getFase();
  }, [LaborId]);

  useEffect(() => {
    getProceso(FaseId);
  }, [FaseId]);

  useEffect(() => {
    let { status, fuente, message } = responseFirebase;
    if (status) {
      limpiarElementosTodos();
      if (fuente === "server") {
        enqueueSnackbar("Parte Diario Registrado Correctamente", {
          variant: "success",
        });
      } else if (fuente === "Local") {
        limpiarElementosTodos();
        enqueueSnackbar("Parte Diario Registrado Correctamente; Modo Local", {
          variant: "info",
        });
      }
    } else {
      enqueueSnackbar("Error: " + message, {
        variant: "error",
      });
    }
  }, [responseFirebase]);

  useEffect(() => {
    getUser((data) => {
      setSedeId(data.sede_id);
      setUsuarioId(data.usuario_id);
      setUsuarioName(data.name);
      getLabores();
      setTipoProyectoId("P");

      inicializarLocalizacion();
    });
  }, []);

  const inicializarLocalizacion = async () => {
    obtenerUbicacion((resolve) => {
      if (resolve.estaPermitido) {
        setLocalizacion(resolve.posicion);
      } else {
        enqueueSnackbar(SENTENCIAS[21], {
          variant: "error",
        });
      }
    });
  };

  const limpiarElementosTodos = () => {
    setDataCultivo([]);
    setDataCompania([]);
    setDataFundo([]);
    setDataLote([]);
    setDataProceso([]);

    setCultivoId("");
    setCompaniaId("");
    setFundoId("");
    setLote("");
    setLoteId("");
    setLaborId("");
    setFaseId("");
    setProcesoId("");
    setnumeroRepaso(0);
    setTurno("T");
  };

  return (
    <>
      <FormControl sx={{ flexGrow: 1, width: "100%" }}>
        <Typography
          textAlign={"left"}
          style={{ color: "#717873" }}
          variant="h5"
          component="h5"
        >
          <AddTask /> Crear Parte Diario
        </Typography>

        {localizacion.length == 0 ? (
          <Alert severity="error">{SENTENCIAS[21]}</Alert>
        ) : (
          void 0
        )}

        <Divider />
        <br />
        <Grid container spacing={2}>
          <Grid item xs={12} md={3}>
            <FormLabel id="demo-row-radio-buttons-group-label">
              Tipo de Proyecto
            </FormLabel>
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
              value={tipoProyectoId}
              onChange={(i, o) => setTipoProyectoId(o)}
            >
              <FormControlLabel
                value="P"
                control={<Radio />}
                label="Producción"
              />
              <FormControlLabel
                value="I"
                control={<Radio />}
                label="Inversión"
              />
              {/* <FormControlLabel
          value="disabled"
          disabled
          control={<Radio />}
          label="other"
        /> */}
            </RadioGroup>
          </Grid>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`(${dataCultivo.length}) Cultivo`}
              Namelabel={"cultivo"}
              IdLabel={"cultivo_id"}
              getSelected={(item, object) => {
                console.log(item.cultivo_id);
                setCultivoId(item.cultivo_id);
              }}
              valueInput={""}
              data={dataCultivo}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`(${dataCompania.length}) Campaña`}
              Namelabel={"campania"}
              IdLabel={"campania_id"}
              getSelected={(item, object) => {
                setCompaniaId(item.campania_id);
              }}
              valueInput={""}
              data={dataCompania}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`(${dataFundo.length}) Fundo`}
              Namelabel={"fundo_id"}
              IdLabel={"fundo_id"}
              getSelected={(item, object) => {
                setFundoId(item.fundo_id);
              }}
              valueInput={""}
              data={dataFundo}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`Lote`}
              Namelabel={"lote"}
              IdLabel={"lote"}
              getSelected={(item, object) => {
                setLote(item.lote);
                setLoteId(object.lote_id);
              }}
              valueInput={""}
              data={dataLote}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Asynchronous
              label={`Labores`}
              Namelabel={"labor"}
              IdLabel={"labor_id"}
              getSelected={(item, object) => {
                setLaborId(item.labor_id);
              }}
              valueInput={""}
              data={dataLabores}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <InputWithLector
              label={"N° de Repaso"}
              setvaluerealTime={(state) => {
                setnumeroRepaso(state);
              }}
              state={numeroRepaso}
              ispulsekey={() => {}}
              scanner={false}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`Fase`}
              Namelabel={"fase"}
              IdLabel={"fase_id"}
              getSelected={(item, object) => {
                setFaseId(item.fase_id);
              }}
              valueInput={""}
              data={dataFase}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`Proceso`}
              Namelabel={"proceso"}
              IdLabel={"proceso_id"}
              getSelected={(item, object) => {
                setProcesoId(item.proceso_id);
              }}
              valueInput={""}
              data={dataProceso}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <FormControlLabel
              value="start"
              control={<Checkbox />}
              label="Turno Tarde"
              labelPlacement="start"
              onChange={(e) =>
                e.target.checked ? setTurno("T") : setTurno("M")
              }
            />
          </Grid>
          <Grid sx={{mb:10}} item xs={12} md={3}>
            {localizacion.length > 0 ? (
              <Button
                onClick={crearParteDiario}
                color="error"
                variant="contained"
                endIcon={<Send />}
              >
                Continuar
              </Button>
            ) : (
              void 0
            )}
          </Grid>
        </Grid>
        
      </FormControl>

      <CompBackdrop isopen={openBackdrop} mensaje={mensajeBackdrop} />
    </>
  );
}
