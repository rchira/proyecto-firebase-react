import {
  Avatar,
  Badge,
  Button,
  Chip,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Tooltip,
} from "@mui/material";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import SaveAsIcon from '@mui/icons-material/SaveAs'
import ContentPasteIcon from '@mui/icons-material/ContentPaste'
import SyncLockIcon from '@mui/icons-material/SyncLock'
import { useEffect, useState } from "react";

import {
  delNavegador,
  estaOpcionExisteEnElObjeto,
  getUser,
} from "../../utils/helpers";
import metodos from "../../server/metodos";
import { CAMPO } from "../../server/constantes/campos";
import { format } from "date-fns";
import { styled } from "@mui/material/styles";
import { useLocation, useNavigate } from "react-router-dom";
import ThreeDots from "../../components/ContentLoader/Article";
import { EstadoRegistro } from "../../components/EstadoRegistro";
import { ListLabores } from "./componentes/ListaLabores";
import CloseFullscreenIcon from "@mui/icons-material/CloseFullscreen";
import SettingsIcon from "@mui/icons-material/Settings";
import { Box } from "@mui/system";
import { ConfirmDialog } from "../../components/ConfirmDialog";
import { useSnackbar } from "notistack";
import { useModalWithData } from "../../hooks/useModalWithData";
import AlertDialogSlide from "../../components/AlertDialogSlide/AlertDialogSlide";
import { OpcionesResumen } from "./componentes/OpcionesResumen";
import LockOpenIcon from '@mui/icons-material/LockOpen';
import { goDo, haveDo } from "../../server/funciones";
import { TooltipCustom } from "../../components/TooltipCustom";
const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: "0 4px",
  },
}));
export const ParteDiarios = () => {
  const { state } = useLocation();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  // let isStateExpanded=((state && state.expanded)? state.expanded : false )

  const [user, setUser] = useState({});
  const [fecha, setFecha] = useState(format(new Date(), "yyyy-MM-dd"));
  const [parteDiarios, setParteDiarios] = useState([]);
  const [expanded, setExpanded] = useState(false);
  const [estaTraendoPartes, setEstaTraendoPartes] = useState(false);
  const [setIsModalOpened, isModalOpened, modalData, setModalData] =
    useModalWithData();

  // contiene un componente
  const [COMPONENTE_RENDER_FOR_MODAL, SETCOMPONENTE_RENDER_FOR_MODAL] =
    useState(<></>);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const obtenerPartesDiarios = (usuarioId) => {
    setEstaTraendoPartes(true);
    metodos.pd.listarParteDiariosPorUsuario(true, usuarioId, fecha, (data) => {
      console.log(data);
      setEstaTraendoPartes(false);
      setParteDiarios(data);
    });
  };
  const cerrarParteDiario = async (parte_id) => {
    let resultado = await metodos.pd.cerrarParteDiarioModel(parte_id, {});
    console.log(resultado);
    let { habilitadoPersonalenLabores, habilitadoRendimientosPersonalenLabor } =
      resultado || {};

    if (
      habilitadoPersonalenLabores !== undefined &&
      !habilitadoPersonalenLabores
    ) {
      enqueueSnackbar("Labores vacias de Personal", {
        variant: "error",
      });
    }
    if (
      habilitadoRendimientosPersonalenLabor !== undefined &&
      !habilitadoRendimientosPersonalenLabor
    ) {
      enqueueSnackbar("Falta registrar rendimientos del Personal", {
        variant: "error",
      });
    }
    /**se muestra el mensaje del registro */
    if (resultado.status !== undefined && resultado.status) {
      enqueueSnackbar("Parte Diario Cerrado", {
        variant: "success",
      });
      setIsModalOpened(false);
      SETCOMPONENTE_RENDER_FOR_MODAL(<></>);
    } else if (resultado.status !== undefined && resultado.status == false) {
      enqueueSnackbar("Error al cerrar Parte Diario", {
        variant: "error",
      });
    }
  };

  const eliminarparteDiario = async (parte_id) => {
    let resultado = await metodos.pd.eliminarParteDiarioModel(parte_id, {});
    if (resultado.status) {
      enqueueSnackbar("Parte Diario Eliminado", {
        variant: "success",
      });
      setIsModalOpened(false);
      SETCOMPONENTE_RENDER_FOR_MODAL(<></>);
    } else {
      enqueueSnackbar("Error al eliminar Parte Diario", {
        variant: "error",
      });
    }
  };

  const omitirLaborPorIdLabor = async (pd_id, labor_id) => {
    let cantidaddepersonasenlabor = await new Promise((resolve, reject) => {
      metodos.pd.listarPersonalPorParteDiario(true, pd_id, labor_id, (data) => {
        resolve(data.length || 0);
      });
    });
    let mensaje = "";
    if (cantidaddepersonasenlabor > 0) {
      mensaje = `La Labor tiene ${cantidaddepersonasenlabor} Personal, ¿Estas Seguro que deseas anular?`;
    } else {
      mensaje = "¿Estas Seguro que deseas anular?";
    }
    console.log("metodo ejcución");
    ConfirmDialog(mensaje, () => {
      metodos.labores.anularLaborModel(pd_id, labor_id, (data) => {
        enqueueSnackbar("Labor Anulado", {
          variant: "success",
        });
      });
    });
  };

  useEffect(() => {
    try {
      getUser((response) => {
        setUser(response);
        console.log("veces");
        obtenerPartesDiarios(response.usuario_id);
      });

    } catch (error) {}
  }, []);

  return (
    <>
      <div>
        {/* <img
          width={"100%"}
          height={300}
          style={{}}
          loading="lazy"
          src={require("../../sources/background.jpg")}
        /> */}

        <div
          style={{
            position: "relative",
            // marginTop: -100,
            width: "90%",
            left: "50%",
            right: "50%",
            marginLeft: "-45%",
            display: "inline-block",
            padding: 5,
          }}
          variant="outlined"
        >
          <Box color={'GrayText'} component='h3'>¡Bienvenido(a)!</Box>
          <span style={{}}>
          <Chip size="medium"  avatar={<Avatar>{(user.name)? user.name.substr(0,1):  void 0}</Avatar>} label={user.name} />
            </span>
          <br></br>
          <Box color={'GrayText'} component='span'>
            {user.sede_id} {"|"}
            {user.usuario_id} {"  |  "}
            {delNavegador("reloadUser")
              ?delNavegador("reloadUser").lastRefreshAt
              : ""}
          </Box>
          <br></br>
          <Box color={'GrayText'} component='span'>{user.email}</Box>
          <br></br>
          <br></br>
        </div>
      </div>
      {estaTraendoPartes ? <ThreeDots /> : void 0}
      {parteDiarios.map((e, i) => (
        <Accordion
          key={e.id}
          expanded={expanded === `panel${i}`}
          onChange={handleChange(`panel${i}`)}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography sx={{ width: "100%", flexShrink: 0 }}>
              <strong>
                N° {e.numero} {e.lote}
                {"-"}
                {e.fundo}
                <EstadoRegistro
                  sx={{ ml: 2 }}
                  fecha={e.fecha_creacion_parte_diario_firestore}
                />
              </strong>
             
              <Box  sx={{ml:2,float:'right'}} component={'strong'}>{e.estaCerrado ?
              <TooltipCustom title={'Parte Diario ya sido cerrado'}><SyncLockIcon  color="error"/></TooltipCustom>  :
              <TooltipCustom title={'Parte Diario Abierto'}><LockOpenIcon /></TooltipCustom> }</Box>
            </Typography>
            {/* <Typography sx={{ width: "50%", flexShrink: 0 }}>
              [NUBE]<br></br>
              <strong>
                {e.estaDescargado ? "[DESCARGADO]" : "[SIN DESCARGAR]"}
              </strong>
            </Typography> */}
          </AccordionSummary>
          <AccordionDetails>
            {/* lista aqui */}
            {expanded && expanded === `panel${i}` ? (
              <ListLabores
                lote={e.lote}
                fundo={e.fundo}
                parte_id={e.id}
                numero={e.numero}
                expanded={`panel${i}`}
                estaCerrado={e.estaCerrado}
                omitirLabor={(parte_id_labor, labor_id_labor) => {
                  omitirLaborPorIdLabor(parte_id_labor, labor_id_labor);
                }}
              />
            ) : (
              void 0
            )}

          <center>
              <Button
                onClick={() => {
                  let obj = {
                    parte_id: e.id,
                    numero_pd: e.numero,
                    lote: e.lote,
                    fundo: e.fundo,
                    usuario: e.usuario_creacion,
                  };
                  SETCOMPONENTE_RENDER_FOR_MODAL(
                    <OpcionesResumen
                      eliminarparteDiario={(pd) => {
                        eliminarparteDiario(pd);
                      }}
                      data={obj}
                      verResumenParteDiarioHandle={(
                        SUBCOMPONENTE_A_RENDERIZAR
                      ) =>
                        SETCOMPONENTE_RENDER_FOR_MODAL(
                          SUBCOMPONENTE_A_RENDERIZAR
                        )
                      }
                    />
                  );
                  setModalData({ active: true });
                  setIsModalOpened(true);
                }}
                sx={{ mr: 3 }}
                color="error"
                size="small"
              >
                <SettingsIcon />
                Opciones
              </Button>
              {!e.estaCerrado ? (
                <Button
                  onClick={() => {
                    ConfirmDialog(`¿Deseas cerrar parte diario ahora?`, () => {
                      cerrarParteDiario(e.id);
                    });
                  }}
                  variant="contained"
                  color="error"
                  size="small"
                  endIcon={<SaveAsIcon />}
                >
                  <CloseFullscreenIcon />
                  Finalizar/ Cerrar/ Enviar Parte Diario
                </Button>
              ) : (
                void 0
              )}
            </center>
          </AccordionDetails>
        </Accordion>
      ))}
      <Box sx={{ mb: 10 }}></Box>
      {modalData ? (
        <AlertDialogSlide
          isopen={isModalOpened}
          handleClose={() => {
            setIsModalOpened(false);
          }}
          cerrarModalMedianteBoton={() => {
            setIsModalOpened(false);
          }}
        >
          {COMPONENTE_RENDER_FOR_MODAL}
        </AlertDialogSlide>
      ) : (
        void 0
      )}
    </>
  );
};
