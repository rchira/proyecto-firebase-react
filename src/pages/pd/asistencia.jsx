import { useNavigate, useLocation } from "react-router-dom";
import { useEffect, useRef, useState } from "react";
import {
  Button,
  Card,
  Container,
  Grid,
  Typography,
  Alert,
  AlertTitle,
  IconButton,
  Box,
} from "@mui/material";
import ToolbarGrid from "../../components/DataGrid/DataGrid";
import MaterialUIPickers from "../../components/Date/date";
import GroupAddIcon from "@mui/icons-material/GroupAdd";
import BorderAllIcon from "@mui/icons-material/BorderAll";
import SendIcon from "@mui/icons-material/Send";
import PlaylistAddCheckOutlinedIcon from "@mui/icons-material/PlaylistAddCheckOutlined";
import { CompBackdrop } from "../../components/Backdrop";
import { useModalWithData } from "../../hooks/useModalWithData";
import AlertDialogSlide from "../../components/AlertDialogSlide/AlertDialogSlide";
import InputWithLector from "../../components/InputWithLector";
import metodos from "../../server/metodos";
import { CAMPO } from "../../server/constantes/campos";
import { PATH } from "../../utils/path";
import { SENTENCIAS } from "../../utils/sentencias";
import CardList from "../../components/ContentLoader/UpworkJobLoader";
import InfoOutlined from "@mui/icons-material/InfoOutlined";
import { useSnackbar } from "notistack";
import { columns } from "./Columnas/personalAsistencia_columnas";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
export function Asitencia() {
  /**referencias */
  // let cajaDocumento = useRef(null);
  /**alertas */
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  /** */
  const { state } = useLocation();
  const [parteId, setParteId] = useState(
    state && state.parte_id ? state.parte_id : 0
  );
  
  const [estaCerrado, setestaCerrado] = useState(
    state && state.estaCerrado ? state.estaCerrado : false
  );

  const [laborId, setLaborId] = useState(
    state && state.labor_id ? state.labor_id : 0
  );
  const [numeroParteDiario, setNumeroParteDiario] = useState(
    state && state.numero_parte ? state.numero_parte : 0
  );
  const [expanded, setExpanded] = useState(
    state && state.expanded ? state.expanded : ""
  );
  const [isPrincipal, setIsPrincipal] = useState(
    state && state.principal ? state.principal : 0
  );
  const [labor, setLabor] = useState(state && state.labor ? state.labor : "");
  const [fundo, setFundo] = useState(state && state.fundo ? state.fundo : "");
  const [lote, setLote] = useState(state && state.lote ? state.lote : "");

  const [tiempoInicioParaPD, setTiempoInicioParaPD] = useState(
    new Date().getTime()
  );

  const [isopenBackdrop, setIsopenBackdrop] = useState(false);
  const [mensajeBackDrop, setmensajeBackDrop] = useState("");

  const [setIsModalOpened, isModalOpened, modalData, setModalData] =
    useModalWithData();

  const [documentoPersonal, setDocumentoPersonal] = useState("");
  const [infoPersonalEncontrado, setInfoPersonalEncontrado] = useState({});
  const [fuisteAconsultarPersonal, setFuisteAconsultarPersonal] = useState("");
  const [listaPersonal, setListaPersonal] = useState([]);

  const guardarPersonal = (restricciones = {}, cerrarModal) => {
    if (tiempoInicioParaPD) {
      setIsopenBackdrop(true);
      setmensajeBackDrop(SENTENCIAS[14]);
      metodos.personal.registrarPersonalenParteDiarioModel(
        {
          [CAMPO.PARTEDIARIOID]: parteId,
          [CAMPO.LABORID]: laborId,
          [CAMPO.NUMERODOCUMENTO]: documentoPersonal,

          [CAMPO.ESACTIVO]: true,
          [CAMPO.ESEXTERNO]: false,
          [CAMPO.ESPRINCIPAL]: isPrincipal,
          [CAMPO.INICIO]: tiempoInicioParaPD,
        },
        restricciones,
        (data) => {
          setIsopenBackdrop(false);
          setmensajeBackDrop("");
          let { huboModificacion, descripcion, parteId_donde_trabajo } = data;
          if (huboModificacion) {
            enqueueSnackbar(SENTENCIAS[10], {
              variant: "success",
            });
            setDocumentoPersonal("");
            if (cerrarModal) {
              setIsModalOpened(false);
            }
          } else {
            enqueueSnackbar(SENTENCIAS[11], {
              variant: "error",
            });
          }
          console.log(data);
        }
      );
    } else {
      enqueueSnackbar(SENTENCIAS[9], {
        variant: "warning",
      });
    }
  };

  const verificarPersonal = async (value) => {
    setFuisteAconsultarPersonal(SENTENCIAS[12]);
    metodos.personal.obtenerPersonalPorNumero(false, value, (data) => {
      setInfoPersonalEncontrado(data[0]);
      setFuisteAconsultarPersonal(SENTENCIAS[13]);
    });
  };

  const obtenerListaPersonal = () => {
    metodos.pd.listarPersonalPorParteDiario(true, parteId, laborId, (data) => {
      let lPersonal = data || [];
      let dataSet = lPersonal.map((e) => ({ ...e, laborId, parteId,estaCerrado }));
      setListaPersonal(dataSet);
    });
  };

  useEffect(() => {
    obtenerListaPersonal();
  }, []);

  const navigate = useNavigate();
  /*
  if (parteId == 0) {
    navigate(PATH.PARTEDIARIO);
  }
 */

  return (
    <>
      
        <Grid container spacing={2}>
          <Grid item xs={3} md={1}>
            <IconButton
              onClick={() =>
                navigate(PATH.PARTEDIARIO, {
                  state: {
                    expanded,
                  },
                })
              }
              color="error"
              aria-label="add to shopping cart"
            >
              <ArrowBackIcon />
            </IconButton>
          </Grid>
          <Grid item xs={9} md={11}>
            <Typography variant="h6" component="h6">
              N° {numeroParteDiario}
              <Box sx={{ ml: 3 }} component={"span"}>
                {lote} -{" "}
              </Box>
              <Box component={"span"}>{fundo}</Box>
              <Box component={"div"} fontSize={15} sx={{}}>
                {labor}
              </Box>
            </Typography>
            <br />
          </Grid>
        </Grid>
        {/* hasta aqui */}

        
          <br />
          <Grid container spacing={2}>
            <Grid item xs={12} md={2}>
              <MaterialUIPickers
                label={"Tiempo"}
                value={tiempoInicioParaPD}
                handleChange={(fecha) => {
                  setTiempoInicioParaPD(fecha);
                }}
                type={2}
              />
            </Grid>
            <Grid item xs={12} md={4}>
              {parteId && !estaCerrado ? (
                <Button
                  color="error"
                  variant="contained"
                  startIcon={<GroupAddIcon />}
                  onClick={() => {
                    setIsModalOpened(true);
                  }}
                >
                  Registrar Colaboradores
                </Button>
              ) : (
                void 0
              )}
            </Grid>
            <Grid item xs={12} md={4}></Grid>
          </Grid>
 <Card sx={{ p: 2, mt: 5, }}>   
        <Typography
          sx={{
            display: "flex",
            alignItems: "center",
          }}
          variant="h6"
          component="h6"
        >
          <BorderAllIcon /> Resumen
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            Total de Personas. {listaPersonal.length}
          </Grid>
          <Grid item xs={12} md={4}>
            Con Fotocheck. [Sin Calcular]
          </Grid>
          <Grid item xs={12} md={4}>
            Sin Fotocheck. [Sin Calcular]
          </Grid>
        </Grid>
        </Card>

      <Typography
        sx={{
          display: "flex",
          marginTop: 5,
          alignItems: "center",
        }}
        variant="h6"
        component="h6"
      >
        <PlaylistAddCheckOutlinedIcon /> Lista de Personal
      </Typography>
      {listaPersonal.length > 0 ? (
        <ToolbarGrid
          keyRow={"numero_documento"}
          configColumns={columns}
          data={listaPersonal}
        />
      ) : (
        false
      )}

      <AlertDialogSlide
        isopen={isModalOpened}
        handleClose={() => {
          //  cuando queremos cerrar
          //  por si queremos validar
        }}
        cerrarModalMedianteBoton={() => {
          setIsModalOpened(false);
        }}
      >
        <Typography
          sx={{
            display: "flex",
            marginTop: 5,
            alignItems: "center",
          }}
          variant="h6"
          component="h6"
        >
          <PlaylistAddCheckOutlinedIcon />
          Agregar Nuevo Personal
        </Typography>

        <Grid sx={{ mt: 5 }} container spacing={2}>
          <Grid item xs={12} md={12}>
            <InputWithLector
              // ref={cajaDocumento}
              label="Numero de Documento"
              scanner={true}
              validalongitud={8}
              state={documentoPersonal}
              setvaluerealTime={(a) => {
                setDocumentoPersonal(a);
              }}
              clickEscanearDni={() => {
                alert("Lo sentimos, aun no esta habilitada esta opción.");
              }}
              ispulsekey={() => {}}
              estaCumplidaLongitud={(value) => {
                verificarPersonal(value);
              }}
            />
          </Grid>

          {fuisteAconsultarPersonal === "consultando" ? <CardList /> : void 0}

          {fuisteAconsultarPersonal == "si" ? (
            documentoPersonal.length === 8 ? (
              infoPersonalEncontrado && infoPersonalEncontrado.personal_id ? (
                <>
                  <Grid item xs={12} md={12}>
                    <Typography component="span">
                      <strong>DNI: </strong>
                      {infoPersonalEncontrado.numero_documento}
                    </Typography>
                  </Grid>
                  <Grid item xs={12} md={12}>
                    <Typography component="span">
                      <strong>Nombre:</strong> {infoPersonalEncontrado.personal}
                    </Typography>
                  </Grid>
                  <Grid item xs={12} md={12}>
                    <Typography component="span">
                      <strong>Sede :</strong>{" "}
                      {infoPersonalEncontrado.sede_id == "1100"
                        ? "ICA"
                        : "PIURA"}
                    </Typography>
                  </Grid>

                  <Grid item xs={12} md={12} container spacing={1}>
                    <Button
                      color="error"
                      variant="outlined"
                      startIcon={<GroupAddIcon />}
                      sx={{ mr: 1, mt: 1 }}
                      onClick={() =>
                        guardarPersonal({ estaPersonal: true }, true)
                      }
                    >
                      Agregar a la lista
                    </Button>
                    <Button
                      sx={{ mt: 1 }}
                      color="error"
                      variant="contained"
                      startIcon={<GroupAddIcon />}
                      onClick={() => guardarPersonal({ estaPersonal: true })}
                    >
                      Agregar y añadir a otro Personal
                    </Button>
                  </Grid>
                </>
              ) : (
                <>
                  <Grid item xs={12} md={12}>
                    <Card sx={{ p: 2 }}>
                      <center>
                        <InfoOutlined sx={{ fontSize: 80 }} />
                      </center>
                      <strong>{SENTENCIAS[7]}</strong>
                      {SENTENCIAS[8][0]} {documentoPersonal} {SENTENCIAS[8][1]}
                      <Grid sx={{ mt: 2 }} container spacing={2}>
                        <Grid item xs={12} md={12}>
                          <center>
                            <Button
                              onClick={() => setDocumentoPersonal("")}
                              variant="outlined"
                              color="error"
                            >
                              No
                            </Button>
                            <Button
                              sx={{ ml: 2 }}
                              variant="contained"
                              color="error"
                              onClick={() => {
                                guardarPersonal({ estaPersonal: false });
                              }}
                            >
                              Si
                            </Button>
                          </center>
                        </Grid>
                      </Grid>
                    </Card>
                  </Grid>
                </>
              )
            ) : (
              void 0
            )
          ) : (
            void 0
          )}
        </Grid>
      </AlertDialogSlide>

      <CompBackdrop mensaje={mensajeBackDrop} isopen={isopenBackdrop} />

      {/* <Button onClick={() => navigate(-1)} variant="contained" endIcon={<SendIcon />}>
  Regresar
</Button> */}
    </>
  );
}
