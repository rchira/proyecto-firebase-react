import { Button, Card, Divider, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import Asynchronous from "../../components/Autocomplete";
import InputWithLector from "../../components/InputWithLector";
import PersonSearchIcon from "@mui/icons-material/PersonSearch";
import ToolbarGrid from "../../components/DataGrid/DataGrid";
import metodos from "../../server/metodos";
import { CAMPO } from "../../server/constantes/campos";
import { COMPARACION } from "../../server/constantes/comparaciones";
import { obtenerSedesUsuariosModel } from "../../server/metodos/usuarios";
import { columns } from "./columnas/listarUsuariosColumnas";
export function ListarUsuarios() {
  const [numeroDocumento, setNumeroDocumento] = useState("");
  const [sedes, setSedes] = useState([]);
  const [dataUsuarios, setDataUsuarios] = useState([]);

  const obtenerSedes = async () => {
    let resultado = await obtenerSedesUsuariosModel();
    setSedes(resultado);
  };

  const obtenerUsuarios = (queries) => {
    metodos.usuarios.listarUsuariosPorFiltradores(true,queries, (data) => {
      console.log(data);
      setDataUsuarios(data);
    });
  };
  useEffect(() => {
    obtenerUsuarios([]);
    obtenerSedes();
  }, []);
  return (
    <>
      <Typography
        textAlign={"left"}
        style={{ color: "#717873" }}
        variant="h5"
        component="h5"
      >
        <PersonSearchIcon /> Usuarios
      </Typography>
      <Card sx={{ p: 2 }}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`(${sedes.length}) Sedes`}
              Namelabel={"sede_id"}
              IdLabel={"sede_id"}
              getSelected={(item, object) => {
                obtenerUsuarios([
                    {
                      column: CAMPO.SEDEID,
                      operator: COMPARACION.IGUAL,
                      value: item.sede_id,
                    },
                  ]);
              }}
              valueInput={""}
              data={sedes}
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <InputWithLector
            placeholder="Dni Usuario"
              state={numeroDocumento}
              setvaluerealTime={(value) => {
                setNumeroDocumento(value);
              }}
              label={"Documento"}
              ispulsekey={() => {
                obtenerUsuarios([
                  {
                    column: CAMPO.EMAIL,
                    operator: COMPARACION.IGUAL,
                    value: `${numeroDocumento}@app.pdlc.com`,
                  },
                ]);
              }}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            {/* <Button variant="contained" color="success">Consultar</Button> */}
          </Grid>
        </Grid>
        <Divider></Divider>
        <Grid item xs={12} md={12}>
          <ToolbarGrid
            keyRow={"id"}
            configColumns={columns}
            data={dataUsuarios}
          />
        </Grid>
      </Card>
    </>
  );
}
