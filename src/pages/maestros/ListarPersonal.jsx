import { Button, Card, Divider, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import Asynchronous from "../../components/Autocomplete";
import InputWithLector from "../../components/InputWithLector";
import { obtenerSedesPersonalModel } from "../../server/metodos/personal";
import PersonSearchIcon from "@mui/icons-material/PersonSearch";
import ToolbarGrid from "../../components/DataGrid/DataGrid";
import metodos from "../../server/metodos";
import { columns } from "./columnas/listarPersonalcolumnas";
import { CAMPO } from "../../server/constantes/campos";
import { COMPARACION } from "../../server/constantes/comparaciones";
export function ListarPersonal() {
  const [numeroDocumento, setNumeroDocumento] = useState("");
  const [sedes, setSedes] = useState([]);
  const [dataPersonal, setDataPersonal] = useState([]);

  const obtenerSedes = async () => {
    let resultado = await obtenerSedesPersonalModel();
    setSedes(resultado);
  };

  const ListarPersonal = (queries) => {
    metodos.personal.listarPersonalPorFiltradores(false,queries, (data) => {
      setDataPersonal(data);
    });
  };
  useEffect(() => {
    ListarPersonal([]);
    obtenerSedes();
  }, []);
  return (
    <>
      <Typography
        textAlign={"left"}
        style={{ color: "#717873" }}
        variant="h5"
        component="h5"
      >
        <PersonSearchIcon /> Personal
      </Typography>
      <Card sx={{ p: 2 }}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={3}>
            <Asynchronous
              label={`(${sedes.length}) Sedes`}
              Namelabel={"sede_id"}
              IdLabel={"sede_id"}
              getSelected={(item, object) => {
                ListarPersonal([
                    {
                      column: CAMPO.SEDEID,
                      operator: COMPARACION.IGUAL,
                      value: item.sede_id,
                    },
                  ]);
              }}
              valueInput={""}
              data={sedes}
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <InputWithLector
            placeholder="Dni Personal"
              state={numeroDocumento}
              setvaluerealTime={(value) => {
                setNumeroDocumento(value);
              }}
              label={"Numero Personal"}
              ispulsekey={() => {
                ListarPersonal([
                  {
                    column: CAMPO.NUMERODOCUMENTO,
                    operator: COMPARACION.IGUAL,
                    value: numeroDocumento,
                  },
                ]);
              }}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            {/* <Button variant="contained" color="success">Consultar</Button> */}
          </Grid>
        </Grid>
        <Divider></Divider>
        <Grid item xs={12} md={12}>
          <ToolbarGrid
            keyRow={"numero_documento"}
            configColumns={columns}
            data={dataPersonal}
          />
        </Grid>
      </Card>
    </>
  );
}
