import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useEffect, useState } from "react";
import { alNavegador, validarEmail } from "../../utils/helpers";
import { goDo } from "../../server/funciones";
import { useNavigate } from "react-router-dom";
import { PATH } from "../../utils/path";
import { useSnackbar } from "notistack";
import metodos from "../../server/metodos";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="/">
        PEDREGAL
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const theme = createTheme();

export default function SingInUser({ handleActiveLogin }) {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  let navigate = useNavigate();

  const [user, setuser] = useState("");
  const [password, setPassword] = useState("");
  const [isEnabledButton, setIsEnabledButton] = useState(true);

  const getInfoUserSesion = (email) => {
    goDo.queryCollectionByParams(
      "usuarios",
      {
        column: "email",
        operator: "==",
        value: email,
      },
      (dt) => {
        let { sede_id,usuario_id } = dt[0] || [];
        alNavegador("user", dt);
        navigate(PATH.SINCRONIZACION, {
          state: {
            aproved: true,
            sede_id,
            usuario_id
          },
        });
      }
    );
  };

  const processSignIn = (event) => {
    event.preventDefault();
      metodos.usuarios.iniciarSesionModel(user, password, (status, r) => {
        if (status) {
          if (r) {
            // handleActiveLogin(true)
            setuser("")
            setPassword("")
            alNavegador("reloadUser", r.user.reloadUserInfo);
            getInfoUserSesion(r.user.email);
          }
        } else {
          enqueueSnackbar("Usuario Y/O clave Incorrecta", {
            variant: "error",
          });
        }
      });

  
  };

  useEffect(() => {
    if (validarEmail(user)) {
      let dni_in_email = user.split("@")[0];
      if (password === dni_in_email) {
        setIsEnabledButton(false);
      } else {
        setIsEnabledButton(true);
      }
    }
  }, [user, password]);

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: "url(https://pe.joblum.com/uploads/1/351.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "error.main" }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Iniciar Sesión
            </Typography>
            <Box
              component="form"
              noValidate
              onSubmit={processSignIn}
              sx={{ mt: 1 }}
            >
              <TextField
                value={user}
                onChange={(e) => setuser(e.target.value)}
                margin="normal"
                required
                fullWidth
                id="email"
                label="Usuario"
                name="email"
                autoComplete="email"
                autoFocus
              />
              <TextField
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                margin="normal"
                required
                fullWidth
                name="password"
                label="Contraseña"
                type="password"
                id="password"
                autoComplete="current-password"
              />
              {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
              <Button
                disabled={isEnabledButton}
                type="submit"
                fullWidth
                variant="contained"
                color="error"
                sx={{ mt: 3, mb: 2 }}
              >
                Ingresar
              </Button>
              {/* <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid> */}
              <Copyright sx={{ mt: 5 }} />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
