import LoadingButton from "@mui/lab/LoadingButton";
import Box from "@mui/material/Box";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import SaveIcon from "@mui/icons-material/Save";
import SendIcon from "@mui/icons-material/Send";
import { useEffect, useState } from "react";
import metodos from "../../server/metodos";
import { useSnackbar } from "notistack";
import { getUser } from "../../utils/helpers";
import { format } from "date-fns";
import { Alert, Typography } from "@mui/material";
import { AddTask } from "@mui/icons-material";
export function ActualizarCache() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  /**varibales momentanias */
  const [sedeId, setSedeId] = useState(0);
  const [usuarioId, setUsuarioId] = useState(0);
  const [usuarioName, setUsuarioName] = useState("");

  const [loading, setLoading] = useState({
    loadingPersonal: false,
    loadingParteDiario: false,
  });

  useEffect(() => {
    getUser((data) => {
      setSedeId(data.sede_id);
      setUsuarioId(data.usuario_id);
      setUsuarioName(data.name);
    });
  }, []);

  const ObtenciondeTodoElParteDiario = async () => {
    setLoading({ ...loading, loadingParteDiario: true });

    let partesDiarios = await new Promise((resolve, reject) => {
      metodos.pd.listarParteDiariosPorUsuario(
        true,
        usuarioId,
        format(new Date(), "yyyy-MM-dd"),
        (data) => {
          resolve(data);
        }
      );
    });

    let cantidadPartesDiarios = partesDiarios.length || 0;

    if (cantidadPartesDiarios > 0) {
      let contadorPartesDiarios = 0;

      for (const pd of partesDiarios) {
        contadorPartesDiarios += 1;

        /* obtenemos los partes diarios personal por el id del parte diario*/

        let parte_diario_personal = await new Promise((resolve, reject) => {
          metodos.pd.listarParteDiarioPersonal(true, pd.id, (data) => {
            resolve(data);
          });
        });

        let labores = await new Promise((resolve, reject) => {
          metodos.pd.listarLaboresPorParteDiario(true, pd.id, (data) => {
            resolve(data);
          });
        });

        let cantidadLabores = labores.length || 0;
        let contadorLabores = 0;
        /**obtenemos el personal por labor: */
        for (const lbs of labores) {
          contadorLabores += 1;

          let personal = await new Promise((resolve, reject) => {
            metodos.pd.listarPersonalPorParteDiario(
              true,
              pd.id,
              lbs.labor_id,
              (data) => {
                resolve(data);
              }
            );
          });

          /**aqui analizamos para redireccionar */
          if (
            (contadorPartesDiarios =
              cantidadPartesDiarios && contadorLabores == cantidadLabores)
          ) {
            setLoading({ ...loading, loadingParteDiario: false });
            enqueueSnackbar("Parte Diarios, Descargados ", {
              variant: "success",
            });
          }
        }
      }
    } else {
      setLoading({ ...loading, loadingParteDiario: false });
      enqueueSnackbar("Parte Diarios, Descargados", {
        variant: "success",
      });
    }
  };
  const descargarPersonal = async () => {
    setLoading({ ...loading, loadingPersonal: true });
    await new Promise((resolve, reject) => {
      metodos.personal.obtenerPersonal(true, (data) => {
        setLoading({ ...loading, loadingPersonal: false });
        resolve(data);
      });
    });
    enqueueSnackbar("Coleccion Personal Actualizado", {
      variant: "success",
    });
  };

  return (
    <Box>
      <Typography
        textAlign={"left"}
        style={{ color: "#717873" }}
        variant="h5"
        component="h5"
      >
        <AddTask /> Sincronizar Información con el Servidor
      </Typography>
      <Alert severity="warning">
        Se necesita Tener Internet, para hacer la sincronización de forma
        correcta.
      </Alert>
      <FormControlLabel
        sx={{
          display: "block",
        }}
        control={
          <Switch
            checked={false}
            onChange={() => setLoading(!loading)}
            name="loading"
            color="primary"
          />
        }
        label="Descargar Todo"
      />
      <Box sx={{ "& > button": { m: 1 } }}>
        <LoadingButton
          color="error"
          size="small"
          onClick={descargarPersonal}
          endIcon={<SendIcon />}
          loading={loading.loadingPersonal}
          loadingPosition="end"
          variant="outlined"
        >
          Descargar Personal
        </LoadingButton>

        <LoadingButton
          color="error"
          size="small"
          onClick={ObtenciondeTodoElParteDiario}
          endIcon={<SendIcon />}
          loading={loading.loadingParteDiario}
          loadingPosition="end"
          variant="outlined"
        >
          Sincronizar mis Parte Diarios
        </LoadingButton>
        {/* <LoadingButton
            size="small"
            color="secondary"
            onClick={handleClick}
            loading={loading}
            loadingPosition="start"
            startIcon={<SaveIcon />}
            variant="contained"
          >
            Save
          </LoadingButton> */}
      </Box>
    </Box>
  );
}
