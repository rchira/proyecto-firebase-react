import { async } from "@firebase/util";
import { format } from "date-fns";
import { useEffect, useState } from "react";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { CompBackdrop } from "../../components/Backdrop";
import metodos from "../../server/metodos";
import { alNavegador } from "../../utils/helpers";
import { PATH } from "../../utils/path";
import { SENTENCIAS } from "../../utils/sentencias";

export function Sincronizacion({ handleActiveLogin }) {
  let navigate = useNavigate();
  const { state } = useLocation();
  const [aproved, setAproved] = useState(state && state.aproved ? true : false);
  const [sede_id, setSede_Id] = useState(
    state && state.sede_id ? state.sede_id : 0
  );
  const [usuario_id, setUsuario_Id] = useState(
    state && state.usuario_id ? state.usuario_id : 0
  );
  const [isopenBackdrop, setIsopenBackdrop] = useState(true);
  const [mensajeBackDrop, setmensajeBackDrop] = useState("");

  /**transaccionales */

  const ObtenciondeTodoElParteDiario = async () => {
    setmensajeBackDrop(SENTENCIAS[24]);

    let partesDiarios = await new Promise((resolve, reject) => {
      metodos.pd.listarParteDiariosPorUsuario(
        true,
        usuario_id,
        format(new Date(), "yyyy-MM-dd"),
        (data) => {
          resolve(data);
        }
      );
    });

    let cantidadPartesDiarios = partesDiarios.length || 0;

    if (cantidadPartesDiarios > 0) {
      let contadorPartesDiarios = 0;

      for (const pd of partesDiarios) {
        contadorPartesDiarios += 1;

        /* obtenemos los partes diarios personal por el id del parte diario*/
        setmensajeBackDrop(`${SENTENCIAS[27]} de:  [${pd.id}]`);
        let parte_diario_personal = await new Promise((resolve, reject) => {
          metodos.pd.listarParteDiarioPersonal(true, pd.id, (data) => {
            resolve(data);
          });
        });

        setmensajeBackDrop(`${SENTENCIAS[25]} :  [${pd.id}]`);

        let labores = await new Promise((resolve, reject) => {
          metodos.pd.listarLaboresPorParteDiario(true, pd.id, (data) => {
            resolve(data);
          });
        });

        let cantidadLabores = labores.length || 0;
        let contadorLabores = 0;
        /**obtenemos el personal por labor: */
        for (const lbs of labores) {
          contadorLabores += 1;
          setmensajeBackDrop(`${SENTENCIAS[26]} :  [${lbs.labor_id}]`);
          let personal = await new Promise((resolve, reject) => {
            metodos.pd.listarPersonalPorParteDiario(
              true,
              pd.id,
              lbs.labor_id,
              (data) => {
                resolve(data);
              }
            );
          });

          /**aqui analizamos para redireccionar */
          if (
            (contadorPartesDiarios =
              cantidadPartesDiarios && contadorLabores == cantidadLabores)
          ) {
            redireccionando();
          }
        }
      }
    } else {
      redireccionando();
    }
  };

  /**Descargamos las tablas transaccionales */

  const redireccionando = () => {
    setmensajeBackDrop(SENTENCIAS[6]);
    setTimeout(() => {
      window.location.href = PATH.PARTEDIARIO;
      // handleActiveLogin(true)
      // navigate(PATH.PARTEDIARIO);
    }, 2000);
  };

  useEffect(async () => {
    // listamos el personal
    await new Promise((resolve, reject) => {
      setmensajeBackDrop(SENTENCIAS[3]);
      metodos.personal.obtenerPersonal(true, (data) => {
        setmensajeBackDrop(SENTENCIAS[2]);
        resolve(data);
      });
    });

    // obtenerMaestros
    await new Promise((resolve, reject) => {
      setmensajeBackDrop(SENTENCIAS[4]);
      metodos.maestros.obtenerMaestros(true, sede_id, (data) => {
        setmensajeBackDrop(SENTENCIAS[5]);
        resolve(data);
      });
    });

    // getLabores
    await new Promise((resolve, reject) => {
      setmensajeBackDrop(SENTENCIAS[15]);
      metodos.labores.obtenerLabores(true, (data, where) => {
        setmensajeBackDrop(SENTENCIAS[16]);
        resolve(data);
      });
    });

    // getElementosPeps
    await new Promise((resolve, reject) => {
      setmensajeBackDrop(SENTENCIAS[28]);
      metodos.programaciones.listarElementosPeps(
        true,
        format(new Date(), "yyyy-MM-dd"),
        (data, where) => {
          resolve(data);
        }
      );
    });
    // obtenerLaboresSecundarias
    await new Promise((resolve, reject) => {
      setmensajeBackDrop(SENTENCIAS[17]);
      metodos.labores.obtenerLaboresSecundarias(true, (data) => {
        setmensajeBackDrop(SENTENCIAS[18]);
        resolve(data);
      });
    });

    await new Promise((resolve, reject) => {
      setmensajeBackDrop(SENTENCIAS[22]);
      metodos.fases.obtenerFasesProcesos(true, sede_id, (data) => {
        setmensajeBackDrop(SENTENCIAS[23]);
        /** llamo una funcion transaccional a guardar */
        resolve(data);
      });
    });

    ObtenciondeTodoElParteDiario()
  }, []);

  if (!aproved) {
    return <Navigate to={PATH.INICIARSESION} />;
  }
  return (
    <>
      <CompBackdrop isopen={isopenBackdrop} mensaje={mensajeBackDrop} />
    </>
  );
}
