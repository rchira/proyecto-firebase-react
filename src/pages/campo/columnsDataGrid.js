export const columns = [
    {
      field: 'labor_id',
      headerName: 'LABOR ID',
      width: 100,
      editable: true,
    },
    {
      field: 'labor',
      headerName: 'LABOR ',
      width: 350,
      editable: true,
    },
  
    {
      field: 'numero_documento',
      headerName: 'DNI ',
      width: 80,
      editable: true,
    },
    {
      field: 'personal',
      headerName: 'PERSONAL ',
      width: 350,
      editable: true,
    },
    { field: 'personal_id', headerName: 'PERSONAL ID', width: 90 },
   
   {
      field: 'sede_id',
      headerName: 'SEDE',
      width: 110,
      editable: true,
    },
    {
    field: 'sede',
    headerName: 'SEDE',
     description: '',
    sortable: false,
       width: 160,
       valueGetter: (params) =>
       `${params.row.sede_id=='1100'?'Ica': 'Piura'}`,
    },
  ];
  