import { useEffect, useState } from "react";
import config from "./config.json";
import Quagga from "quagga";
import './scanner.css'
function Scanner({ isCamera, onDetected }) {
  const [_scannerIsRunning, set_scannerIsRunning] = useState(false);
  const startScanner = () => {
    Quagga.init({
      inputStream: {
        name: "Live",
        type: "LiveStream",
        constraints: {
          width: 640,
          height: 480,
          facingMode: "environment",
          deviceId: "default"
        },
        area: { // defines rectangle of the detection/localization area
          top: "0%",    // top offset
          right: "0%",  // right offset
          left: "0%",   // left offset
          bottom: "0%"  // bottom offset
        },
        singleChannel: false, // true: only the red color-channel is read
        target: document.querySelector('#interactive')    // Or '#yourElement' (optional)
      },
      numOfWorkers: 4,
      locate: true,
      frequency: 10,
      locator: {
        halfSample: true,
        patchSize: "medium", // x-small, small, medium, large, x-large
        debug: {
          showCanvas: false,
          showPatches: false,
          showFoundPatches: false,
          showSkeleton: false,
          showLabels: false,
          showPatchLabels: false,
          showRemainingPatchLabels: false,
          boxFromPatches: {
            showTransformed: false,
            showTransformedBox: false,
            showBB: false
          }
        }
      },
      debug: false,
      decoder: {
        readers: [
          "code_128_reader",

          "ean_8_reader",
          "code_39_reader",
          "code_39_vin_reader",
          "codabar_reader",
          "upc_reader",
          "upc_e_reader",
          "i2of5_reader",
          "2of5_reader",
          "code_93_reader",
          {
            format: "ean_reader",
            config: {
              supplements: [
                'ean_5_reader', 'ean_2_reader'
              ]
            }
          }
        ],
        debug: {
          drawBoundingBox: false,
          showFrequency: false,
          drawScanline: false,
          showPattern: false
        },
        multiple: false
      }
    }, function (err) {
      if (err) {
        console.log(err);
        return
      }
      console.log("Initialization finished. Ready to start");
      Quagga.start();

      set_scannerIsRunning(true)
    });

    Quagga.onProcessed(function (result) {
      var drawingCtx = Quagga.canvas.ctx.overlay,
        drawingCanvas = Quagga.canvas.dom.overlay;

      if (result) {
        if (result.boxes) {
          drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
          result.boxes.filter(function (box) {
            return box !== result.box;
          }).forEach(function (box) {
            Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, { color: "green", lineWidth: 2 });
          });
        }

        if (result.box) {
          Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, { color: "#00F", lineWidth: 2 });
        }

        if (result.codeResult && result.codeResult.code) {
          Quagga.ImageDebug.drawPath(result.line, { x: 'x', y: 'y' }, drawingCtx, { color: 'red', lineWidth: 3 });
        }
      }
    });
    Quagga.onDetected(function (result) {
      onDetected(result.codeResult.code)
      // console.log("Barcode detected and processed : [" + result.codeResult.code + "]", result);
    });
  }


  useEffect(() => {
    if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {   
    if (isCamera) {
      startScanner()
    } else {
      if (_scannerIsRunning) {
        Quagga.stop();
      }
    }
  }
  }, [isCamera])


  return (
    // If you do not specify a target,
    // QuaggaJS would look for an element that matches
    // the CSS selector #interactive.viewport
    <>
    <div id="interactive" className="viewport" />
    {
      (!_scannerIsRunning)?
      <center><p>Por favor espere un momento</p></center>
      :  void 0
    }
    </>
  );
};

export default Scanner;
