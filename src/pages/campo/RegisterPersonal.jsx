


import React, { useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { Container, Divider, Grid, Typography, Paper, Button, Backdrop, CircularProgress } from "@mui/material";
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import Asynchronous from '../../components/Autocomplete';
import InputWithLector from '../../components/InputWithLector';
import ToolbarGrid from '../../components/DataGrid/DataGrid'
import { useModalWithData } from '../../hooks/useModalWithData';
import { useSnackbar } from 'notistack';
import AlertDialogSlide from '../../components/AlertDialogSlide/AlertDialogSlide';
import { AsistenciaPersonal } from './AsistenciaPersonal';
import Scanner from './scanner/Scanner';
import { removeDuplicates } from '../../utils/helpers';
import { columns } from './columnsDataGrid';
import { goDo } from '../../server/funciones';


const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(2),
    boxShadow: 'none',
    //  textAlign: 'center',
    color: theme.palette.text.secondary,

}));



export function RegisterPersonal({ isopen, isclose }) {


    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const [
        setIsModalOpened,
        isModalOpened,
        modalData,
        setModalData
    ] = useModalWithData()

    const [isStop, setIsStop] = useState(false);
    const [isCamera, setIsCamera] = useState(false);

    const [users, setUsers] = useState([]);
    const [progress, setProgress] = useState(true);
    const [ofwhere, setOfWhere] = useState('');
    const [dataLabores, setDataLabores] = useState([]);
    const [dataUsuarioLabor, setDatausuarioLabor] = useState([]);
    const [personal, setPersonal] = useState([]);
    const [dni, setDni] = useState([]);
    const [namePersonal, setnamePersonal] = useState([]);
    const [isdisabledbuttonRegister, setisdisabledbuttonRegister] = useState(false);

    /** informacion guardada para registros */
    const [dGPersonal, setdGPersonal] = useState({});
    const [dGLabor, setdGLabor] = useState({});





    const saveRegister = () => {
        try {
            let { labor, labor_id } = dGLabor;
            let { personal, personal_id, numero_documento, sede_id } = dGPersonal;
            let state = { personal, personal_id, numero_documento, sede_id, labor, labor_id }
            if (labor_id > 0 && personal_id > 0) {
                goDo.setDocument('usuarioLabor/' + personal_id, state, (data) => {
                    console.log(data);
                })
            }
        } catch (error) {

        }
    }

    const queryForDni = (dni) => {
        if (dni.length == 8) {
            return [[...personal].find((e) => { return e.numero_documento == dni })]
        }
    }

    const checkButton = () => {
        if (dni.length == 8 && namePersonal.length > 0) {
            setisdisabledbuttonRegister(false)
        }
    }



    useEffect(() => {
        isopen()
        goDo.queryAllCollection('labores', (data, ofwhere) => {
            setDataLabores(removeDuplicates(data,'labor'))
        })
        goDo.queryAllCollection('personal', (data, ofwhere) => {
            setPersonal(data)
        })
        goDo.queryAllCollection('usuarioLabor', (data, ofwhere) => {
            setDatausuarioLabor(data)
            isclose()
        })

    }, [])


    return (
        <>
            {/* <Container> */}
            <Typography textAlign={'center'} style={{ color: '#717873' }} variant="h4" component="h5">
                Asignar Labor al Personal

            </Typography>

            {/* {
          (ofwhere) ?
            <Alert>
              <AlertTitle>Data Traida</AlertTitle>
              La data se a traido de {ofwhere}
            </Alert>
            : void 0
        } */}


            <br />
            <Box sx={{ flexGrow: 1, width: '100%' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12} md={4}>
                        <Item>

                            <Asynchronous
                                label={`(${dataLabores.length}) Labores`}
                                Namelabel={"labor"}
                                IdLabel={"labor_id"}
                                getSelected={(item) => {
                                    setdGLabor(item)
                                }}
                                data={dataLabores} />

                        </Item>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Item>
                            <InputWithLector

                                label='DNI PERSONAL'
                                name='personal'
                                ispulsekey={() => {
                                    //cuando se presiona una tecla
                                    let obj = queryForDni(dni)
                                    if (obj.length > 0) {
                                        let name = (obj[0]) ? obj[0].personal : 'Personal No Existe'
                                        setnamePersonal(name)
                                        setdGPersonal(obj[0])
                                    }

                                }}
                                setvaluerealTime={(value) => {
                                    //cuando se esta escribiendo en el input
                                    setDni(value)
                                }}
                                state={dni}
                                clickEscanearDni={() => {
                                    setIsCamera(true)
                                    setIsModalOpened(true)

                                }}
                            />

                            <label>{namePersonal}</label>
                        </Item>

                    </Grid>
                    <Grid item xs={12} md={2}>
                        <Item>

                            <Button onClick={saveRegister} disabled={isdisabledbuttonRegister} variant="contained" color="success">
                                Registrar
                            </Button>
                        </Item>
                    </Grid>

                    <Divider />
                    <Grid item xs={12} md={12}>
                        <Item>
                            <ToolbarGrid
                                  configColumns={columns}
                                  keyRow={'personal_id'}
                                data={dataUsuarioLabor} />
                        </Item>
                    </Grid>

                </Grid>
            </Box>
            {/* </Container> */}

            <AlertDialogSlide
                isopen={isModalOpened}
                handleClose={() => {
                    setIsCamera(false)
                    setIsStop(true)
                    setIsModalOpened(false)
                }}
            >

                <Scanner
                    isStop={isStop}
                    isCamera={isCamera}
                    onDetected={(dni) => {

                        if (dni && dni.length > 0) {
                            let obj = queryForDni(dni)
                            if (obj.length > 0) {
                                setDni(dni)
                                let name = (obj[0]) ? obj[0].personal : 'Personal No Existe'
                                setnamePersonal(name)

                                setdGPersonal(obj[0])
                                setIsCamera(false)
                                setIsModalOpened(false)
                            }
                        }
                    }} />



            </AlertDialogSlide>
        </>
    )
}