export const COLLECCION= {
    LABORES:'labores',
    FASESPROCESOS:'fases_procesos',
    FASES:'fases',
    LABORESSECUNDARIAS:'labores_secundarias',
    MAESTROS:'maestros',
    TABLAS:'tablas',
    PARTEDIARIO:'parte_diario',
    PARTEDIARIOPERSONAL:'parte_diario_personal',
    PERSONAL:'personal',
    PROGRAMACIONES:'programaciones',
    ELEMENTOSPEPS:'elementos_peps',
    QUASPARTEDIARIO:'qas_parte_diario',
    SINCRONIZACION:'sincronizacion',
    USUARIOS:'usuarios',
    USUARIOSLABOR:'usuarioLabor',
    QASPARTEDIARIOPERSONAL:'qas_parte_diario_personal',
    MAESTROSTABLAS:'maestros_tablas',
    
}