import * as goDo from "./functions"
import * as haveDo from "./funcionesTransaccional"

export {
    goDo,/**f. callback asincronos realtime*/
    haveDo /**f. son promesas que solo se ejcutan una vez,evento escucha desactivado */
}