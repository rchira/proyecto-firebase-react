// Import the functions you need from the SDKs you need
import { auth, fireStore } from "../coneccion/config";

import {
    addDoc,
    collection,
    doc,
    getDoc,
    limit,
    onSnapshot,
    QuerySnapshot,
    setDoc,
    where,
    deleteDoc,
    query,
    Timestamp,
    serverTimestamp,
    updateDoc
} from 'firebase/firestore'
import { onAuthStateChanged, signInWithEmailAndPassword } from "firebase/auth";

let unsubscribe = [];



async function isLoggedIn() {
    await onAuthStateChanged(auth, (user) => {
        if (user) {
            console.log(user);
            // User is signed in, see docs for a list of available properties
            // https://firebase.google.com/docs/reference/js/firebase.User
            // const uid = user.uid;
            return {
                status: true,
                user
            }
        } else {
            return {
                status: false,
                user
            }
        }
    });

}
function SignIn(email, password, message) {
    signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {

            message(true,userCredential)
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            // ..
            message(false,errorCode,errorMessage)
        });

}


async function deleteDocument(collection, documentKey, resolve) {
    /**nota: no borra susbcolecciones dentro del documento */
    const refDoc = doc(fireStore, collection, documentKey)
    const rs = await deleteDoc(refDoc);
    resolve(rs)
}

//  function probando() {
//     const col = doc(fireStore, "quas_parteDiario/nNEwWbkuv8rN048uKLJg/labores/38")
//     setDoc(col, {
//         name: "Luis",
//         age: 25,
//         noteOne: 20,
//         noteTwo: 18
//     }, { merge: true, includeMetadataChanges: true })
// }
//  probando()
/** asigna el documento con la key document que le pasamos */
function setDocument(nameCollectionWithKey, params, resolve) {
    const refDoc = doc(fireStore, nameCollectionWithKey)
    const id = refDoc.id
    //el atributo merge, actualiza la data del id si lo existe
    setDoc(refDoc, params, { merge: true, includeMetadataChanges: true })
        .then(() => {
            resolve(true, 'server', 'This value has been written to the database', id)
        })
        .catch((error) => {
            resolve(false, 'server', `I got an error! ${error}`)
        })
    onSnapshot(refDoc, (doc) => {
        const source = doc.metadata.hasPendingWrites ? "Local" : "Server";
        resolve(true, source, '',)
    });

}





async function CreateNewDocument(nameColection, params = {}, resolve) {
    const col = collection(fireStore, nameColection);
    const id = col.id

    addDoc(col, params)
        .then((docRef) => {
            resolve(true, 'server', `Your doc was created  at ${docRef.path}`, docRef.id)
        })
        .catch((error) => {
            resolve(false, 'server', `I got an error! ${error}`)
        })

    /**retorna un objeto con datos especificatorios de la insercion */
    resolve(true, 'Local', `fue creado de modo local`, id)
}


async function readASingleDocument(nameCollectionWithKey, resolve) {
    const doc = doc(fireStore, nameCollectionWithKey)
    const mySnapshot = await getDoc(doc);
    if (mySnapshot.exists()) {
        const docData = mySnapshot.data()
        resolve(docData, `My data is  ${JSON.stringify(docData)}`)
    }
}

/** ESCUCHA EVENTOS Y DISPARA EN TIEMPO REAL, ACTUALIZANDO LA DATA EN EL VIEWPORT */
function listenToDocument(nameCollectionWithKey, resolve) {
    const doc = doc(fireStore, nameCollectionWithKey)
    unsubscribe[nameCollectionWithKey] = onSnapshot(doc, docSnapshot => {
        if (docSnapshot.exists()) {
            const docData = docSnapshot.data()
            resolve(docData, `In realtime, docData is ${JSON.stringify(docData)}`)
        }
    })
}

function cancelMyListenerAtTheAppropriateTime(name) {
    unsubscribe[name]()
}



/** start => online and offline */
async function queryAllCollection(nameCollection, resolve) {
    console.log(nameCollection);
    const dataQuery = query(collection(fireStore, nameCollection));
    unsubscribe[nameCollection] = onSnapshot(
        dataQuery, { includeMetadataChanges: true },
        (QuerySnapshot) => {
            const source = QuerySnapshot.metadata.fromCache ? "local-cache" : "server";
            resolve((QuerySnapshot.docs.map((e) => e.data())), source);
        }
    )
}


function queryCollectionByParams(nameCollection, { column, operator, value }, resolve) {
    const ref = collection(fireStore, nameCollection);
    const dataQuery = query(ref, where(column, operator, value));
    unsubscribe[nameCollection] = onSnapshot(
        dataQuery, { includeMetadataChanges: true },
        (QuerySnapshot) => {
            const source = QuerySnapshot.metadata.fromCache ? "local-cache" : "server";
            resolve((QuerySnapshot.docs.map((e) => { return { ...e.data(), id: e.id } })), source);
        }
    )
}

async function queryCollectionByParamsByMoreQuery(nameCollection, queries = [], resolve) {
    const ref = collection(fireStore, nameCollection);
    const dataQuery = query(ref, ...queries.map(e => where(e.column, e.operator, e.value)));
    unsubscribe[nameCollection] = onSnapshot(
        dataQuery, { includeMetadataChanges: true },
        (QuerySnapshot) => {
            const source = QuerySnapshot.metadata.fromCache ? "local-cache" : "server";
            resolve((QuerySnapshot.docs.map((e) => { return { ...e.data(), id: e.id } })), source);
        }
    )
}


export {
    deleteDocument,
    setDocument,
    CreateNewDocument,
    readASingleDocument,
    listenToDocument,
    queryAllCollection,
    queryCollectionByParams,
    cancelMyListenerAtTheAppropriateTime,
    queryCollectionByParamsByMoreQuery,
    SignIn,
    isLoggedIn,
    Timestamp,
    serverTimestamp
}