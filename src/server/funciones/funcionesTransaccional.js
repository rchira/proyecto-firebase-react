// Import the functions you need from the SDKs you need
import { auth, fireStore } from "../coneccion/config";

import {
  addDoc,
  collection,
  doc,
  getDoc,
  limit,
  onSnapshot,
  QuerySnapshot,
  setDoc,
  where,
  deleteDoc,
  query,
  Timestamp,
  serverTimestamp,
  updateDoc,
  getDocFromCache,
  getDocsFromCache,
  getDocFromServer,
} from "firebase/firestore";
import { onAuthStateChanged, signInWithEmailAndPassword } from "firebase/auth";
import { hayConeccion } from "../../utils/helpers";
import { async } from "@firebase/util";
let time=2000

async function actualizarUnDocumento(nameColection, params = {}) {
  const ref = doc(fireStore, nameColection);
  const id = ref.id;
  const path = ref.path;
  const pr1 = new Promise((resolve, reject) => {
    updateDoc(ref, params)
      .then(() => {
        resolve({
          status: true,
          whereIs: "server",
          path: path,
          id: id,
        });
      })
      .catch((error) => {
        resolve({
          status: false,
          whereIs: "server",
          err: `Tienes un error ${error}`,
        });
      });
  });

  const pr2 = new Promise((resolve, reject) => {
    resolve({ status: true, whereIs: "local", path, id });
  });

  const resultPromise = await Promise.race([pr1, pr2]);
  return resultPromise;
}

async function crearDocumento(nameColection, params = {}) {
  const col = collection(fireStore, nameColection);
  const id = col.id;
  const path = col.path;

  const pr1 = new Promise((resolve, reject) => {
    addDoc(col, params)
      .then((docRef) => {
        resolve({
          status: true,
          whereIs: "server",
          path: docRef.path,
          id: docRef.id,
        });
      })
      .catch((error) => {
        resolve({
          status: false,
          whereIs: "server",
          err: `Tienes un error ${error}`,
        });
      });
  });

  const pr2 = new Promise((resolve, reject) => {
    // if (!hayConeccion()) {
    resolve({ status: true, whereIs: "local", path, id });
    // }
  });

  const resultPromise = await Promise.race([pr1, pr2]);
  return resultPromise;
}

async function establecerDocumento(nameColection, params = {}) {
  const col = doc(fireStore, nameColection);
  const id = col.id;
  const path = col.path;

  const pr1 = new Promise((resolve, reject) => {
    setDoc(col, params, { merge: true, includeMetadataChanges: true })
      .then(() => {
        resolve({
          status: true,
          whereIs: "server",
          path: path,
          id: id,
        });
      })
      .catch((error) => {
        resolve({
          status: false,
          whereIs: "server",
          err: `Tienes un error ${error}`,
        });
      });
  });

  const pr2 = new Promise((resolve, reject) => {
    // if (!hayConeccion()) {
    resolve({ status: true, whereIs: "local", path, id });
    // }
  });

  const resultPromise = await Promise.race([pr1, pr2]);
  return resultPromise;
}

async function consultarColeccion(
  nameCollection,
  debeTenerInternet = false,
  callback
) {
  const dataQuery = query(collection(fireStore, nameCollection));
  if (debeTenerInternet) {
    onSnapshot(dataQuery, { includeMetadataChanges: true }, (QuerySnapshot) => {
      const source = QuerySnapshot.metadata.fromCache
        ? "local-cache"
        : "server";
      callback({ data: QuerySnapshot.docs.map((e) => e.data()), source });
    });
  } else {
    const QuerySnapshot = await getDocsFromCache(dataQuery);
    callback({
      data: QuerySnapshot.docs.map((e) => e.data()),
      source: "local-cache",
    });
  }

  /*
   */
}

async function consultarDocumento(
  nameColectionwithDocumentKey,
  debeTenerInternet = false,
  callback
) {
  const ref_doc = doc(fireStore, nameColectionwithDocumentKey);
  // if(debeTenerInternet){

  // }else{
  try {

    const docu = await getDocFromCache(ref_doc);
    const data = docu.data();

   
      callback({ data, source: "Local" });
 
    
    
  } catch (e) {
    onSnapshot(ref_doc, (documento) => {
      const source = documento.metadata.hasPendingWrites
      ? "Local"
      : "Server";
      const data = documento.data();
      callback({ data, source });
    })

    setTimeout(() => {
      callback({ data:{}, source: "Local" });
    }, time);
  }

  // }
}

async function consultarColeccionPorFiltraciones(
  nameCollection,
  queries = [],
  debeTenerInternet = false,
  callback
) {
  const ref = collection(fireStore, nameCollection);
  const ref_query = query(
    ref,
    ...queries.map((e) => where(e.column, e.operator, e.value))
  );

  if (debeTenerInternet) {
    let retornoOnSnapshot=false
    onSnapshot(ref_query, { includeMetadataChanges: true }, (QuerySnapshot) => {

      const source = QuerySnapshot.metadata.fromCache
        ? "local-cache"
        : "server";
      const data = QuerySnapshot.docs.map((e) => ({ ...e.data(), id: e.id }));
      retornoOnSnapshot=true
      callback({ data, source });
  
    })
    
    if(retornoOnSnapshot){
      const QuerySnapshot = await getDocsFromCache(ref_query);
      const data = QuerySnapshot.docs.map((e) => ({...e.data(), id: e.id }))
  
      setTimeout(async() => {
        callback({ data, source: "local-cache" });
      }, time);
    }
    

  } else {
    try {
      const QuerySnapshot = await getDocsFromCache(ref_query);

    const data = QuerySnapshot.docs.map((e) => ({...e.data(), id: e.id }))


    callback({ data, source: "local-cache" });
    } catch (error) {
      callback({ data:[], source: "local-cache" });
    }
  }
}

async function iniciarSesion(email, password,callback) {

  signInWithEmailAndPassword(auth, email, password)
  .then((userCredential) => {

    callback(true,userCredential)
  })
  .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      // ..
      callback(false,errorCode,errorMessage)
  });


}


async  function estaAutentificado(){
  return await  new Promise((resolve,reject)=>{
    onAuthStateChanged(auth, (user) => {
     
      if (user) {
       resolve({status:true,user})
      } else {
        resolve({status:false,user:{}})
      }
    });
  })
}



export {
  estaAutentificado,
  iniciarSesion,
  consultarColeccion,
  consultarColeccionPorFiltraciones,
  crearDocumento,
  actualizarUnDocumento,
  establecerDocumento,
  consultarDocumento,
  Timestamp,
  serverTimestamp,
};
