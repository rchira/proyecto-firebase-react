import *  as pd from './pd'
import *  as maestros from './maestros'
import *  as labores from './labores'
import *  as fases from './fases'
import *  as personal from './personal'
import *  as usuarios from './usuarios'
import *  as programaciones from './programaciones'
export default  {
    pd,
    maestros,
    labores,
    fases,
    personal,
    usuarios,
    programaciones
}