import { getIdAutogenerado, removeDuplicates } from "../../../utils/helpers";
import { CAMPO, PALABRACLAVE } from "../../constantes/campos";
import { COLLECCION } from "../../constantes/colecciones";
import { COMPARACION } from "../../constantes/comparaciones";
import { goDo, haveDo } from "../../funciones";

export async function obtenerFases(debeTenerInternet,sedeId, callback) {
  haveDo.consultarColeccion(
    `${COLLECCION.FASES}/${sedeId}/${COLLECCION.FASESPROCESOS}`,
    debeTenerInternet,
    (resolve) => {
      console.log(resolve);
      const data = removeDuplicates(resolve.data, CAMPO.FASE) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerFasesProcesos(debeTenerInternet,sedeId,callback) {
  haveDo.consultarColeccion(
    `${COLLECCION.FASES}/${sedeId}/${COLLECCION.FASESPROCESOS}`,
    debeTenerInternet,
    (resolve) => {
      const data = removeDuplicates(resolve.data, CAMPO.PROCESO) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerFasesProcesosPorFaseId(debeTenerInternet,sedeId,faseId,callback) {
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.FASES}/${sedeId}/${COLLECCION.FASESPROCESOS}`,
    [
      {
        column: CAMPO.FASEID,
        operator: COMPARACION.IGUAL,
        value: faseId,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = removeDuplicates(resolve.data, CAMPO.PROCESO) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}