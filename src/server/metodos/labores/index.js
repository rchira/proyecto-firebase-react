import { async } from "@firebase/util";
import { getIdAutogenerado, removeDuplicates } from "../../../utils/helpers";
import { CAMPO, PALABRACLAVE } from "../../constantes/campos";
import { COLLECCION } from "../../constantes/colecciones";
import { COMPARACION } from "../../constantes/comparaciones";
import { goDo, haveDo } from "../../funciones";
import {
  eliminarPersonalPorParteDiario,
  listarPersonalPorParteDiario,
} from "../pd";

export async function anularLaborModel(parte_id, labor_id, callback) {
  let resultados = {};
  resultados.removerPersonal={}
  let personal_agregado_en_labor = await new Promise((resolve, reject) => {
    listarPersonalPorParteDiario(false, parte_id, labor_id, (data) => {
      resolve(data || []);
    });
  });

  personal_agregado_en_labor.forEach(async (e) => {
    resultados.removerPersonal[e.numero_documento] = await eliminarPersonalPorParteDiario(
      parte_id,
      labor_id,
      e.id);
  })
    
      
  
  resultados.estadoAnulacionLabor = await anularLaborPorId(parte_id, labor_id);
  console.log(resultados.estadoAnulacionLabor);
  callback(resultados);
}

export async function anularLaborPorId(parte_id, labor_id) {
  return await haveDo.actualizarUnDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${parte_id}/${COLLECCION.LABORES}/${labor_id}`,
    {
      [CAMPO.ESOMITIDO]: true,
      [CAMPO.FECHAREGISTROOMITIDO]: haveDo.Timestamp.fromDate(new Date()),
      [CAMPO.FECHAREGISTROOMITIDOFIRESTORE]: haveDo.serverTimestamp(),
    }
  );
}
export async function obtenerLabores(debeTenerInternet, callback) {
  haveDo.consultarColeccion(
    COLLECCION.LABORES,
    debeTenerInternet,
    (resolve) => {
      const data = removeDuplicates(resolve.data, CAMPO.LABOR) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerLaborPrincipal(laborId, callback) {
  haveDo.consultarColeccionPorFiltraciones(
    COLLECCION.LABORES,
    [{ column: CAMPO.LABORID, operator: COMPARACION.IGUAL, value: laborId }],
    false,
    (data) => {
      callback(data);
    }
  );
}

export async function obtenerLaboresSecundariasPorLaborId(laborId, callback) {
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.LABORESSECUNDARIAS}`,
    [
      {
        column: CAMPO.LABORID,
        operator: COMPARACION.IGUAL,
        value: laborId,
      },
    ],
    false,
    (data) => {
      callback(data.data || []);
    }
  );
}

export async function obtenerLaboresSecundarias(debeTenerInternet, callback) {
  haveDo.consultarColeccion(
    `${COLLECCION.LABORESSECUNDARIAS}`,
    debeTenerInternet,
    (data) => {
      callback(data);
    }
  );
}

export async function agregarLaborAParteDiario(
  id,
  laborId,
  params,
  toleranciaTiempo
) {
  console.log(
    `${COLLECCION.QUASPARTEDIARIO}/${id}/${COLLECCION.LABORES}/${laborId}`
  );
  return await haveDo.establecerDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${id}/${COLLECCION.LABORES}/${laborId}`,
    params
  );
}

export async function obtenerLaborPorId(debeTenerInternet, labor_id, callback) {
  haveDo.consultarDocumento(
    `${COLLECCION.LABORES}/${labor_id}`,
    debeTenerInternet,
    (data) => {
      callback(data);
    }
  );
}
export async function EditarAtributoLabor(parte_id,labor_id,params = {}) {
  return await haveDo.establecerDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${parte_id}/${COLLECCION.LABORES}/${labor_id}`,
    params
  );
}

export async function obtenerInformacionXLabor(parte_id,labor_id,callback) {
    haveDo.consultarDocumento(`${COLLECCION.QUASPARTEDIARIO}/${parte_id}/${COLLECCION.LABORES}/${labor_id}`,true,(data)=>{
      callback(data)
    })
}