import { async } from "@firebase/util";
import { format } from "date-fns";
import { getIdAutogenerado, removeDuplicates } from "../../../utils/helpers";
import { SENTENCIAS } from "../../../utils/sentencias";
import { CAMPO, PALABRACLAVE } from "../../constantes/campos";
import { COLLECCION } from "../../constantes/colecciones";
import { COMPARACION } from "../../constantes/comparaciones";
import { goDo, haveDo } from "../../funciones";

export async function listarElementosPeps(debeTenerInternet, fecha, callback) {
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.PROGRAMACIONES}/${fecha}/${COLLECCION.ELEMENTOSPEPS}`,
    [],
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function listarProgramacionPorElementoPep(
  debeTenerInternet,
  fecha,
  elementopep,
  callback
) {
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.PROGRAMACIONES}/${fecha}/${COLLECCION.ELEMENTOSPEPS}`,
    [
      {
        column: CAMPO.ELEMENTOPEP,
        operator: COMPARACION.IGUAL,
        value: elementopep,
      }
    ],
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}
/**
 

export async function obtenerSedesUsuariosModel(){
  return await new Promise((resolve,reject)=>{
    listarUsuariosPorFiltradores(true,[],(data)=>{
      console.log(data);
      resolve(removeDuplicates(data,CAMPO.SEDEID))
    })
  })
}
 */
