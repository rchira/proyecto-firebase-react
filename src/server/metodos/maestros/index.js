import { getIdAutogenerado, removeDuplicates } from "../../../utils/helpers";
import { CAMPO, PALABRACLAVE } from "../../constantes/campos";
import { COLLECCION } from "../../constantes/colecciones";
import { COMPARACION } from "../../constantes/comparaciones";
import { goDo, haveDo } from "../../funciones";

export async function obtenerCultivos(debeTenerInternet,sede, tipoProyectoId, callback) {
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.MAESTROS}/${sede}/${COLLECCION.TABLAS}`,
    [
      {
        column: CAMPO.TIPOPROYECTOID,
        operator: COMPARACION.IGUAL,
        value: tipoProyectoId,
      },
    ],debeTenerInternet,
    (resolve) => {
      const data = removeDuplicates(resolve.data, CAMPO.CULTIVO) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerCompanias(
  debeTenerInternet,
  sede,
  tipoProyecto_Id,
  cultipoId,
  callback
) {
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.MAESTROS}/${sede}/${COLLECCION.TABLAS}`,
    [
      {
        column: CAMPO.TIPOPROYECTOID,
        operator: COMPARACION.IGUAL,
        value: tipoProyecto_Id,
      },
      {
        column: CAMPO.CULTIVOID,
        operator: COMPARACION.IGUAL,
        value: cultipoId,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = removeDuplicates(resolve.data, CAMPO.COMPANIA) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerFundos(
  debeTenerInternet,
  sede,
  tipoProyecto_Id,
  cultipoId,
  companiaId,
  callback
) {
  console.log(tipoProyecto_Id + " .. " + cultipoId + ".." + companiaId + "...");
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.MAESTROS}/${sede}/${COLLECCION.TABLAS}`,
    [
      {
        column: CAMPO.TIPOPROYECTOID,
        operator: COMPARACION.IGUAL,
        value: tipoProyecto_Id,
      },
      {
        column: CAMPO.CULTIVOID,
        operator: COMPARACION.IGUAL,
        value: cultipoId,
      },
      {
        column: CAMPO.COMPANIAID,
        operator: COMPARACION.IGUAL,
        value: companiaId,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = removeDuplicates(resolve.data, CAMPO.FUNDOID) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerLotes(
  debeTenerInternet,
  sede,
  tipoProyecto_Id,
  cultipoId,
  companiaId,
  fundoId,
  callback
) {
  console.log(tipoProyecto_Id + " .. " + cultipoId + ".." + companiaId + "...");
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.MAESTROS}/${sede}/${COLLECCION.TABLAS}`,
    [
      {
        column: CAMPO.TIPOPROYECTOID,
        operator: COMPARACION.IGUAL,
        value: tipoProyecto_Id,
      },
      {
        column: CAMPO.CULTIVOID,
        operator: COMPARACION.IGUAL,
        value: cultipoId,
      },
      {
        column: CAMPO.COMPANIAID,
        operator: COMPARACION.IGUAL,
        value: companiaId,
      },
      {
        column: CAMPO.FUNDOID,
        operator: COMPARACION.IGUAL,
        value: fundoId,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = removeDuplicates(resolve.data, CAMPO.LOTE) || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerMaestros(debeTenerInternet,sede_id, callback) {
  haveDo.consultarColeccion(
   `${COLLECCION.MAESTROS}/${sede_id}/${COLLECCION.TABLAS}`,
   debeTenerInternet,
    (resolve) => {
      const data = resolve.data|| [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}