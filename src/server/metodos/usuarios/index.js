import { async } from "@firebase/util";
import { format } from "date-fns";
import { getIdAutogenerado, removeDuplicates } from "../../../utils/helpers";
import { SENTENCIAS } from "../../../utils/sentencias";
import { CAMPO, PALABRACLAVE } from "../../constantes/campos";
import { COLLECCION } from "../../constantes/colecciones";
import { COMPARACION } from "../../constantes/comparaciones";
import { goDo, haveDo } from "../../funciones";
import { consultarDocumento, estaAutentificado, iniciarSesion } from "../../funciones/funcionesTransaccional";
import { estaCerradoParteDiario, registrarPersonalEnParteDiario } from "../pd";

export async  function estaAutentificadoModel(){
  return await estaAutentificado()
}
export async  function iniciarSesionModel(email,password,callback){
   iniciarSesion(email,password,(status,dt)=>{
     callback(status,dt)
   })
}
export async function listarUsuariosPorFiltradores(debeTenerInternet,queries=[],callback) {
  haveDo.consultarColeccionPorFiltraciones(
   COLLECCION.USUARIOS,
   queries,
   debeTenerInternet,
    (resolve) => {
      const data = resolve.data|| [];
      const source = resolve.source;
      callback(removeDuplicates(data,CAMPO.ID), source);
    }
  );
}

export async function obtenerSedesUsuariosModel(){
  return await new Promise((resolve,reject)=>{
    listarUsuariosPorFiltradores(true,[],(data)=>{
      console.log(data);
      resolve(removeDuplicates(data,CAMPO.SEDEID))
    })
  })
}