import { async } from "@firebase/util";
import { format } from "date-fns";
import {
  formatearLoteIdAElementoPep,
  getIdAutogenerado,
  removeDuplicates,
  uuidv4,
} from "../../../utils/helpers";
import { CAMPO } from "../../constantes/campos";
import { COLLECCION } from "../../constantes/colecciones";
import { COMPARACION } from "../../constantes/comparaciones";
import { goDo, haveDo } from "../../funciones";
import {
  agregarLaborAParteDiario,
  EditarAtributoLabor,
  obtenerLaboresSecundariasPorLaborId,
  obtenerLaborPrincipal,
} from "../labores";

export async function cerrarParteDiarioModel(parte_id, params = {}) {
  let resultado = {
    habilitadoPersonalenLabores: true,
    habilitadoRendimientosPersonalenLabor: true,
  };
  const labores = await new Promise((resolve, reject) => {
    listarLaboresPorParteDiario(false, parte_id, (data) => {
      resolve(data);
    });
  });

  for (const lbrs of labores) {
    if (!lbrs.esOmitido) {
      if (lbrs.principal) {
        let listaPersonaldelLabor = await new Promise((resolve, reject) => {
          listarPersonalPorParteDiario(
            true,
            parte_id,
            lbrs.labor_id,
            (data) => {
              resolve(data);
            }
          );
        });
        for (const lp of listaPersonaldelLabor) {
          if ((lp.fecha_registro_rendimiento || undefined) == undefined) {
            resultado = {
              habilitadoPersonalenLabores:
                resultado.habilitadoPersonalenLabores,
              habilitadoRendimientosPersonalenLabor: false,
            };
            return resultado;
          }
        }
      }
      // parseInt((lbrs.rendimiento_total || 0))
      if ((lbrs.personal_total || 0) == 0) {
        resultado = {
          habilitadoRendimientosPersonalenLabor:
            resultado.habilitadoRendimientosPersonalenLabor,
          habilitadoPersonalenLabores: false,
        };
        return resultado;
      }
    }
  }
  if (
    resultado.habilitadoPersonalenLabores &&
    resultado.habilitadoRendimientosPersonalenLabor
  ) {
    return await EditarAtributosEnParteDiario(parte_id, {
      ...params,
      [CAMPO.ESTACERRADO]: true,
      [CAMPO.FECHACIERRE]: haveDo.Timestamp.fromDate(new Date()),
      [CAMPO.FECHACIERREFIRESTORE]: haveDo.serverTimestamp(),
    });
  }

  /*
 
  */
}

export async function eliminarParteDiarioModel(parte_id, params = {}) {
  return await EditarAtributosEnParteDiario(parte_id, {
    ...params,
    [CAMPO.ESACTIVO]: false,
    [CAMPO.FECHAELIMINACION]: haveDo.Timestamp.fromDate(new Date()),
    [CAMPO.FECHAELIMINACIONPARTEDIARIOFIRESTORE]: haveDo.serverTimestamp(),
  });
}
export async function obtenerResumendelParteDiarioModel(parte_id) {
  let resumen = [];
  const labores = await new Promise((resolve, reject) => {
    listarLaboresPorParteDiario(true, parte_id, (data) => {
      resolve(data);
    });
  });

  /*
  for (const lb of labores) {
    const cantidadPersonas = await new Promise((resolve, reject) => {
      obtenerCantidadPersonalPorParteDiario(
        true,
        parte_id,
        lb.labor_id,
        (data) => {
          resolve(data);
        }
      )
    })
*/

  //   resumen.push({...lb,cantidadPersonas})

  // }
  return labores;
}

export async function todosLosPartesDiariosModel(params = {}, callback) {
  let results = [];

  const partesDiarios = await new Promise((resolve, reject) => {
    listarParteDiariosPorUsuario(
      true,
      params[CAMPO.USUARIOCREACIONID],
      params[CAMPO.FECHA],
      (data) => {
        resolve(data);
      }
    );
  });

  for (const pd of partesDiarios) {
    let cantidadPersonal_Array = [];
    let l = await new Promise((resolve, reject) => {
      listarLaboresPorParteDiario(true, pd.id, (data) => {
        resolve(data);
      });
    });

    for (const lbrs of l) {
      let cantidadPersonalEnParteDiario = await new Promise(
        (resolve, reject) => {
          obtenerCantidadPersonalPorParteDiario(
            true,
            pd.id,
            lbrs.labor_id,
            (dt) => {
              resolve(dt);
            }
          );
        }
      );
      cantidadPersonal_Array.push(cantidadPersonalEnParteDiario);
    }

    results.push({ ...pd, labores: l, cantidadPersonal_Array });
  }

  callback(results);
}

export async function crearParteDiarioModel(params = {}, callback) {
  try {
    let results = {};
    results.historialRegistroLaborPrincipal = {};
    const numeroPd = getIdAutogenerado();
    const parteDiarioKey = uuidv4();
    params = { ...params, [CAMPO.NUMERO]: numeroPd };

    const faseId = params[CAMPO.FASEID];
    const procesoId = params[CAMPO.PROCESOID];
    const fase_proceso_Id = faseId + "" + faseId + "" + procesoId;

    const elementoPep_sinLabor =
      formatearLoteIdAElementoPep(params[CAMPO.LOTEID]) + "." + fase_proceso_Id;

    const laborPrincipal = await new Promise((resolve, reject) => {
      obtenerLaborPrincipal(params[CAMPO.LABORID], (data) => {
        resolve(data);
      });
    });

    const laboresSecundarias = await new Promise((resolve, reject) => {
      obtenerLaboresSecundariasPorLaborId(params[CAMPO.LABORID], (data) => {
        resolve(data);
      });
    });

    results.historialRegistroCabeceraPD = await EditarAtributosEnParteDiario(
      parteDiarioKey,
      params
    );

    let { whereIs, status, path, id } =
      results.historialRegistroCabeceraPD || {};

    if (status) {
      let { labor_id, labor } = laborPrincipal.data
        ? laborPrincipal.data[0]
        : {};
      console.log(laboresSecundarias);

      let listaLaboresparaParteDiario = [
        laborPrincipal.data[0],
        ...laboresSecundarias,
      ];
      console.log(listaLaboresparaParteDiario);
      /*
      listaLaboresparaParteDiario.push(laborPrincipal.data[0]);

      laboresSecundarias.data.forEach((element) => {
        listaLaboresparaParteDiario.push(element);
      });
      */

      /**registramos la labor Principal */
      listaLaboresparaParteDiario.forEach(async (e) => {
        let labor = e.labor || e.labor_secundaria;
        let labor_id = e.labor ? e.labor_id : e.labor_secundaria_id;
        let elementoPepLaborPrincipal = elementoPep_sinLabor + "." + labor_id;
        let isPrincipal = e.labor ? true : false;

        results.historialRegistroLaborPrincipal[labor_id] =
          await agregarLaborAParteDiario(
            id,
            labor_id,
            {
              documento_parte_id: id,
              elemento_pep_id: elementoPepLaborPrincipal,
              esOmitido: false,
              esProgramado: false,
              fecha_creacion_labor: haveDo.Timestamp.fromDate(new Date()),
              fecha_creacion_labor_firestore: haveDo.serverTimestamp(),
              fecha_registro_omitido: null,
              fecha_registro_omitido_firestore: null,
              labor: labor,
              labor_id: labor_id,
              numero_repaso: params[CAMPO.NUMEROREPASO],
              personal_con_fotocheck: "",
              personal_sin_fotocheck: "",
              personal_total: "",
              principal: isPrincipal,
            },
            3
          );
      });
      return await { estado: "resolved", resultado: results };

      /**registramos las labores secundarias */
    }
  } catch (error) {
    return await { estado: "error", resultado: error };
  }
}
/**actualizamos o agregamos todo relacionado al documento de parte diario */
export async function EditarAtributosEnParteDiario(parteDiarioKey, params) {
  return await haveDo.establecerDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${parteDiarioKey}`,
    params
  );
}

export function listarParteDiariosPorUsuario(
  debeTenerInternet,
  usuarioId,
  fechaPd,
  callback
) {
  haveDo.consultarColeccionPorFiltraciones(
    COLLECCION.QUASPARTEDIARIO,
    [
      {
        column: CAMPO.USUARIOCREACIONID,
        operator: COMPARACION.IGUAL,
        value: usuarioId,
      },
      {
        column: CAMPO.FECHA,
        operator: COMPARACION.IGUAL,
        value: fechaPd,
      },
      {
        column: CAMPO.ESACTIVO,
        operator: COMPARACION.IGUAL,
        value: true,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export function listarLaboresPorParteDiario(
  debeTenerInternet,
  parteDiarioId,
  callback
) {
  console.log(parteDiarioId);
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.QUASPARTEDIARIO}/${parteDiarioId}/${COLLECCION.LABORES}`,
    [],
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export function obtenerCantidadPersonalPorParteDiario(
  debeTenerInternet,
  parteDiarioId,
  laborId,
  callback
) {
  console.log(parteDiarioId + "  ---- " + laborId);
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.QUASPARTEDIARIO}/${parteDiarioId}/${COLLECCION.LABORES}/${laborId}/${COLLECCION.PERSONAL}`,
    [
      {
        column: CAMPO.ESACTIVO,
        operator: COMPARACION.IGUAL,
        value: true,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      console.log(resolve);
      const data = resolve.data || [];
      const source = resolve.source;
      const cantidad = data.length;
      callback(cantidad, source);
    }
  );
}
export async function registrarPersonalEnParteDiario(
  parteId,
  laborId,
  params = {}
) {
  return await haveDo.crearDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${parteId}/${COLLECCION.LABORES}/${laborId}/${COLLECCION.PERSONAL}`,
    params
  );
}

export function estaCerradoParteDiario(debeTenerInternet, parte_id, callback) {
  haveDo.consultarDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${parte_id}`,
    debeTenerInternet,
    (data) => {
      if (data.data) {
        if (data.data.estaCerrado) {
          callback(true);
        } else {
          callback(false);
        }
      } else {
        callback(true);
      }
    }
  );
}

export function listarPersonalPorParteDiario(
  debeTenerInternet,
  parte_id,
  labor_id,
  callback
) {
  haveDo.consultarColeccionPorFiltraciones(
    `${COLLECCION.QUASPARTEDIARIO}/${parte_id}/${COLLECCION.LABORES}/${labor_id}/${COLLECCION.PERSONAL}`,
    [
      {
        column: CAMPO.ESACTIVO,
        operator: COMPARACION.IGUAL,
        value: true,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}
export async function eliminarPersonalPorParteDiario(
  parte_id,
  labor_id,
  personal_id,
  callback
) {
  let cantidadPersonal = await new Promise((resolve, reject) => {
    obtenerCantidadPersonalPorParteDiario(true, parte_id, labor_id, (data) => {
      resolve(data);
    })
  })

  await EditarAtributoLabor(parte_id,labor_id,{
    [CAMPO.PERSONALTOTAL]: (parseInt(cantidadPersonal) - 1),
  })

  return await haveDo.actualizarUnDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${parte_id}/${COLLECCION.LABORES}/${labor_id}/${COLLECCION.PERSONAL}/${personal_id}`,
    {
      [CAMPO.ESACTIVO]: false,
      [CAMPO.FECHAELIMINACIONPERONAL]: haveDo.Timestamp.fromDate(new Date()),
      [CAMPO.FECHAELIMINACIONPERSONALFIRESTORE]: haveDo.serverTimestamp(),
    }
  );

}

export function listarParteDiarioPersonal(
  debeTenerInternet,
  parte_id,
  callback
) {
  haveDo.consultarColeccionPorFiltraciones(
    COLLECCION.QASPARTEDIARIOPERSONAL,
    [
      {
        column: CAMPO.PARTEDIARIOID,
        operator: COMPARACION.IGUAL,
        value: parte_id,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}
