import { async } from "@firebase/util";
import { format } from "date-fns";
import { getIdAutogenerado, removeDuplicates } from "../../../utils/helpers";
import { SENTENCIAS } from "../../../utils/sentencias";
import { CAMPO, PALABRACLAVE } from "../../constantes/campos";
import { COLLECCION } from "../../constantes/colecciones";
import { COMPARACION } from "../../constantes/comparaciones";
import { goDo, haveDo } from "../../funciones";
import { consultarDocumento } from "../../funciones/funcionesTransaccional";
import {
  EditarAtributoLabor,
  obtenerInformacionXLabor,
  obtenerLaborPorId,
} from "../labores";
import {
  estaCerradoParteDiario,
  listarPersonalPorParteDiario,
  registrarPersonalEnParteDiario,
} from "../pd";

export async function registrarPersonalenParteDiarioModel(
  params = {},
  restricciones = { estaPersonal: true },
  callback
) {
  let results = {};
  let infoPersonal = {};
  let camposDocumentoPersonal = {};
  results.parteId_donde_trabajo = "";

  let estaPersonalTrabajando = await new Promise((resolve, reject) => {
    estaAccesiblePersonal(false, params[CAMPO.NUMERODOCUMENTO], (data) => {
      resolve(data);
    });
  });

  if (restricciones.estaPersonal) {
    infoPersonal = await new Promise((resolve, reject) => {
      obtenerPersonalPorNumero(false, params[CAMPO.NUMERODOCUMENTO], (data) => {
        resolve(data[0] || {});
      });
    });
  }

  const cantidadPersonasenLaLabor = await new Promise((resolve, reject) => {
    listarPersonalPorParteDiario(
      true,
      params[CAMPO.PARTEDIARIOID],
      params[CAMPO.LABORID],
      (data) => {
        resolve(data.length || 0);
      }
    );
  });

  camposDocumentoPersonal = {
    [CAMPO.ESACTIVO]: true,
    [CAMPO.ESEXTERNO]:
      infoPersonal && infoPersonal[CAMPO.PERSONALID] ? false : true,
    [CAMPO.ESPRINCIPAL]: params[CAMPO.ESPRINCIPAL],
    [CAMPO.FECHAREGISTRO]: haveDo.Timestamp.fromDate(new Date()),
    [CAMPO.FECHAREGISTROASISTENCIA]: haveDo.serverTimestamp(),
    [CAMPO.ID]: params[CAMPO.PARTEDIARIOID],
    [CAMPO.INICIO]: haveDo.Timestamp.fromDate(new Date(params[CAMPO.INICIO])),
    [CAMPO.NUMERODOCUMENTO]:
      infoPersonal[CAMPO.NUMERODOCUMENTO] || params[CAMPO.NUMERODOCUMENTO],
    [CAMPO.PERSONAL]: infoPersonal[CAMPO.PERSONAL] || "Sin nombre",
    [CAMPO.PERSONALID]:
      infoPersonal[CAMPO.PERSONALID] || params[CAMPO.NUMERODOCUMENTO],
    // [CAMPO.FECHACREACIONPERSONALFIRESTORE]:haveDo.serverTimestamp()
  };

  results.parteId_donde_trabajo = estaPersonalTrabajando[CAMPO.PARTEDIARIOID];

  const variablesEstadoPersonal = {
    [CAMPO.ESEXTERNO]: false,
    [CAMPO.FASE]: false,
    [CAMPO.LABORID]: params[CAMPO.LABORID],
    [CAMPO.NUMERODOCUMENTO]: params[CAMPO.NUMERODOCUMENTO],
    [CAMPO.PARTEDIARIOID]: params[CAMPO.PARTEDIARIOID],
    [CAMPO.PDLCPERSONALANTERIORID]:
      estaPersonalTrabajando.pdlc_personal_anterior_id || "",
    [CAMPO.PERSONALID]: 0,
    [CAMPO.SEDEID]: 0,
    [CAMPO.TIENECAMBIOLABOR]: false,
    [CAMPO.TIENERECESO]: false,
    [CAMPO.TIENESALIDA]: false,
  };

  if (!results.parteId_donde_trabajo) {
    results.descripcion = "es nuevo este personal";
    /** significa que este personal es nuevo, recien empesara a trabajar */
    results.huboModificacion = true;
    results.estaregistradopersonal_enpartediario =
      await registrarPersonalEnParteDiario(
        params[CAMPO.PARTEDIARIOID],
        params[CAMPO.LABORID],
        camposDocumentoPersonal
      );

    results.estado_registroenpartediariopersonal =
      await establecerEstadoPersonal(variablesEstadoPersonal);
    /** obtemos la cantidad de personal grabado en una labor */

    EditarAtributoLabor(params[CAMPO.PARTEDIARIOID], params[CAMPO.LABORID], {
      [CAMPO.PERSONALTOTAL]: cantidadPersonasenLaLabor + 1,
    });
  } else {
    const estaCerradoPd = await new Promise((resolve, reject) => {
      estaCerradoParteDiario(false, results.parteId_donde_trabajo, (data) => {
        resolve(data);
      });
    });
    console.log(estaCerradoPd);

    if (estaCerradoPd) {
      results.descripcion = SENTENCIAS[0];
      results.huboModificacion = true;
      results.estaregistradopersonal_enpartediario =
        await registrarPersonalEnParteDiario(
          params[CAMPO.PARTEDIARIOID],
          params[CAMPO.LABORID],
          camposDocumentoPersonal
        );
      results.estado_registroenpartediariopersonal =
        await establecerEstadoPersonal(variablesEstadoPersonal);

      /*cambiamos el el total de personas*/
      EditarAtributoLabor(params[CAMPO.PARTEDIARIOID], params[CAMPO.LABORID], {
        [CAMPO.PERSONALTOTAL]: cantidadPersonasenLaLabor + 1,
      });
    } else {
      results.descripcion = SENTENCIAS[1];
      results.huboModificacion = false;
    }

    /** ya esta laborando, nota: se pasa a verificar si estaCerrado su parte diario */
  }

  callback(results);
}

export async function establecerEstadoPersonal(params = {}) {
  return await haveDo.establecerDocumento(
    `${COLLECCION.QASPARTEDIARIOPERSONAL}/${params[CAMPO.NUMERODOCUMENTO]}`,
    params
  );
}

export async function estaAccesiblePersonal(
  debeTenerInternet,
  numero_documento,
  callback
) {
  haveDo.consultarDocumento(
    `${COLLECCION.QASPARTEDIARIOPERSONAL}/${numero_documento}`,
    debeTenerInternet,
    (data) => {
      callback(data.data || {});
    }
  );
}

export async function obtenerPersonal(debeTenerInternet, callback) {
  haveDo.consultarColeccion(
    COLLECCION.PERSONAL,
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerPersonalPorNumero(
  debeTenerInternet,
  numero,
  callback
) {
  haveDo.consultarColeccionPorFiltraciones(
    COLLECCION.PERSONAL,
    [
      {
        column: CAMPO.NUMERODOCUMENTO,
        operator: COMPARACION.IGUAL,
        value: numero,
      },
    ],
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || {};
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function listarPersonalPorFiltradores(
  debeTenerInternet,
  queries = [],
  callback
) {
  haveDo.consultarColeccionPorFiltraciones(
    COLLECCION.PERSONAL,
    queries,
    debeTenerInternet,
    (resolve) => {
      const data = resolve.data || [];
      const source = resolve.source;
      callback(data, source);
    }
  );
}

export async function obtenerSedesPersonalModel(debeTenerInternet) {
  return await new Promise((resolve, reject) => {
    listarPersonalPorFiltradores(false, [], (data) => {
      resolve(removeDuplicates(data, CAMPO.SEDEID));
    });
  });
}

export async function registrarRendimientodelPersonal(
  parte_id,
  labor_id,
  personal_id,
  params
) {
  let resultado = {};
  let cantidad_personas_con_rendimientos = 0;
  let suma_rendimiento1 = 0;
  let suma_rendimiento2 = 0;
  let promedio_rendimiento1 = 0;
  let promedio_rendimiento2 = 0;

  const parametros = {
    ...params,
    [CAMPO.FECHAREGISTRORENDIMIENTO]: haveDo.Timestamp.fromDate(new Date()),
    [CAMPO.FECHAREGISTRORENDIMIENTOFIRESTORE]: haveDo.serverTimestamp(),
  };



  resultado.estadoregistrorendimiento = await haveDo.establecerDocumento(
    `${COLLECCION.QUASPARTEDIARIO}/${parte_id}/${COLLECCION.LABORES}/${labor_id}/${COLLECCION.PERSONAL}/${personal_id}`,
    parametros
  )

    /**obtenemos los datos de las celda del registro labor del parte diario */
    const listaPersonal = await new Promise((resolve, reject) => {
      listarPersonalPorParteDiario(true, parte_id, labor_id, (data) => {
        resolve(data);
      });
    });
  
    for (const lp of listaPersonal) {
      if(lp.rendimiento){cantidad_personas_con_rendimientos+=1}
      suma_rendimiento1 += parseInt((lp.rendimiento ||0));
      suma_rendimiento2 += parseInt((lp.rendimiento2||0));
    }
    /** sacamos los promedios de los rendimientos */
    promedio_rendimiento1 =
      suma_rendimiento1 / cantidad_personas_con_rendimientos;
    promedio_rendimiento2 =
      suma_rendimiento2 / cantidad_personas_con_rendimientos;

  await EditarAtributoLabor(parte_id, labor_id, {
    [CAMPO.RENDIMIENTOTOTAL]: suma_rendimiento1,
    [CAMPO.RENDIMIENTOTOTAL2]: suma_rendimiento2,
    [CAMPO.PROMEDIORENDIMIENTO]: promedio_rendimiento1,
    [CAMPO.PROMEDIORENDIMIENTO2]: promedio_rendimiento2,
  });

  return resultado;
}
