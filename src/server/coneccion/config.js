// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth,setPersistence,browserSessionPersistence,signInWithEmailAndPassword } from "firebase/auth";
import {

  getFirestore,
  enableIndexedDbPersistence,
  // initializeFirestore, CACHE_SIZE_UNLIMITED
} from "firebase/firestore";


// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
/**
   apiKey: process.env.CREATE_REACT_API_KEY,
  authDomain: process.env.CREATE_REACT_AUTH_DOMAIN,
  //  projectId: process.env.CREATE_REACT_PROJECT_ID_VITE,
  projectId: "red-dragon-303614",
  storageBucket: process.env.CREATE_REACT_STORAGE_BUCKET,
  messagingSenderId: process.env.CREATE_REACT_MESSAGIN_SENDER_ID,
  appId: process.env.CREATE_REACT_APP_ID,
  // measurementId: process.env.CREATE_REACT_MEASUREMENTID
 */
  apiKey: "AIzaSyDNafjL9KNPXFKhIFaaXH3PJE8SQ5My5CM",
  authDomain: "red-dragon-303614.firebaseapp.com",
  projectId: "red-dragon-303614",
  storageBucket: "red-dragon-303614.appspot.com",
  messagingSenderId: "683650588621",
  appId: "1:683650588621:web:bb9b52bedbffc003d16e84",
  measurementId: "G-2F7GXZE2D9"
};




const firebaseApp = initializeApp(firebaseConfig);

const fireStore = getFirestore(firebaseApp);
//  const auth = getAuth(firebaseApp);

enableIndexedDbPersistence(fireStore).catch((err) => {
  if (err.code == "failed-precondition") {
    // Multiple tabs open, persistence can only be enabled
    // in one tab at a a time.
    // ...
    console.log(err);
  } else if (err.code == "unimplemented") {
    // The current browser does not support all of the
    // features required to enable persistence
    // ...
    console.log(err);
  }
});

const auth = getAuth(firebaseApp)




/**le endicamos al firestore ; que inhabilite completamente el proceso de limpieza */
// const firestoreDb = initializeFirestore(app, {
//   cacheSizeBytes: CACHE_SIZE_UNLIMITED
// });

export { fireStore,auth };
