import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import Slide from '@mui/material/Slide';
import { Button, DialogActions, DialogContent } from '@mui/material';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AlertDialogSlide({isopen,handleClose,children,cerrarModalMedianteBoton}) {
  return (
     <Dialog
        open={isopen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
       sx={{zIndex:1201}}
      >
        <DialogContent>

        {children}
        </DialogContent>
        {/* <DialogTitle>{"Use Google's location service?"}</DialogTitle> */}
      
        <DialogActions>
          <Button onClick={()=>cerrarModalMedianteBoton()} >Cancelar</Button>
        </DialogActions>
      </Dialog>
  );
}
