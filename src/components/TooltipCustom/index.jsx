
import Tooltip, { tooltipClasses } from "@mui/material/Tooltip";


import CloudOutlinedIcon from '@mui/icons-material/CloudOutlined';
import CloudOffOutlinedIcon from '@mui/icons-material/CloudOffOutlined';
import { styled } from "@mui/material";

const LightTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
  },
}));

export function TooltipCustom({ title,children }) {
  return (
    <>
   
        <LightTooltip  title={title}>
          {children}
        </LightTooltip>
      
    </>
  );
}
