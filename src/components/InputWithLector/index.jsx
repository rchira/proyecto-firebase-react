import {
  FormControl,
  InputAdornment,
  InputLabel,
  Input,
  Box,
  Button,
  Grid,
  IconButton,
} from "@mui/material";
import AccountCircle from "@mui/icons-material/AccountCircle";
import { PhotoCamera } from "@mui/icons-material";
import { useEffect } from "react";

export default function InputWithLector({
  label,
  name,
  setvaluerealTime,
  state,
  ispulsekey,
  clickEscanearDni,
  scanner = false,
  placeholder = "",
  validalongitud = 0,
  estaCumplidaLongitud,
  // referencia
}) {

  return (
    // <Box sx={{background:'red',p:0}}>
      <FormControl variant="outlined">
        <InputLabel htmlFor="input-with-icon-adornment">{label}</InputLabel>
        <Grid  item sx={{ p: 4}} container >
          <Grid item  xs={10} md={10}>
            <Input
      
            // ref={referencia}
             value={state}
              placeholder={placeholder}
              type="number"
              onChange={(e) => {
                setvaluerealTime(e.target.value);
                if (validalongitud > 0) {
                  if (e.target.value.length == validalongitud) {
                    estaCumplidaLongitud(e.target.value);
                  }
                }
              }}
             
              id={name}
              onKeyPress={(e) => {
                let tecla = document.all ? e.keyCode : e.which;

                if (tecla == 13) {
                  ispulsekey();
                }
              }}
              startAdornment={
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item xs={2} md={2}>
            {scanner ? (
              <IconButton
                onClick={() => clickEscanearDni()}
                color="primary"
                aria-label="upload picture"
                component="span"
              >
                <PhotoCamera />
              </IconButton>
            ) : (
              void 0
            )}
          </Grid>
        </Grid>
      </FormControl>
    // </Box>
  );
}
