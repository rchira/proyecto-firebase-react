import { Skeleton } from "@mui/material";

export function SkeletonComponent(comp){

    if(comp==1){
       return <>
       
       <Skeleton animation="wave" /> <Skeleton />
       <Skeleton width="60%" /><Skeleton width="30%" />
       <Skeleton animation="wave" />
       <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
            <Skeleton animation="wave" height={10} width="80%" />
            <Skeleton
              animation="wave"
              height={10}
              width="80%"
              style={{ marginBottom: 6 }}
              />
               <Skeleton animation="wave" variant="circular" width={40} height={40} />
            
       </>
    }
        
    
}