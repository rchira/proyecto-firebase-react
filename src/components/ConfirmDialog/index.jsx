import { Box, Button, Card } from '@mui/material';
import { confirmAlert } from 'react-confirm-alert'; // Import
import InfoIcon from '@mui/icons-material/Info';
export function ConfirmDialog(
    mensaje,
    callback
){
    confirmAlert({
        customUI: ({onClose }) => {
          return (
            <Card  sx={{ p: 2 }}>
              {/* <h1>Are you sure?</h1> */}
             <center>
               <InfoIcon
               sx={{
                 fontSize:100
               }}
               />
             </center>
              <Box sx={{mb:2}} component='p'>{mensaje}</Box>
            
              <center>
              <Button
                size="small"
                sx={{ mr: 2 }}
                variant="contained"
                color="inherit"
                onClick={onClose}
              >
                Cancelar
              </Button>
              <Button
                size="small"
                variant="contained"
                color="error"
                onClick={() => {
                    callback()
                  onClose();
                }}
              >
                Aceptar
              </Button>
              </center>
            </Card>
          );
        },
      });
}