import { CircularProgress, Backdrop } from "@mui/material";
import { Box } from "@mui/system";

export function CompBackdrop({ isopen, mensaje }) {
  return (
    <>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 2 }}
        open={isopen}
        // onClick={handleClose}
      >
        <CircularProgress color="inherit" />
        <Box
          sx={{
            ml: 5,
          }}
        >
          {" "}
          <h3>{mensaje}</h3>
        </Box>
      </Backdrop>
    </>
  );
}
