import * as React from 'react';
import Modal from '@mui/material/Modal';
export default function CustomModal({isopen,handleClose,children}) {
  return (
    <div>
      <Modal
    
        open={isopen}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        
      >
       {children}
      </Modal>
    </div>
  );
}
