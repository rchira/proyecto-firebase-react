import { useEffect, useState } from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import RestoreIcon from "@mui/icons-material/Restore";
import AutoStoriesIcon from "@mui/icons-material/AutoStories";
import AppRegistrationIcon from "@mui/icons-material/AppRegistration";
import CloudSyncIcon from "@mui/icons-material/CloudSync";
import {
  Avatar,
  Badge,
  BottomNavigation,
  BottomNavigationAction,
  Collapse,
  ListItemButton,
  ListSubheader,
  Menu,
  MenuItem,
  Paper,
  Skeleton,
  Tooltip,
} from "@mui/material";
import {
  ExpandLess,
  ExpandMore,
  Logout,
  PersonAdd,
  Settings,
  StarBorder,
} from "@mui/icons-material";
import { Link, useNavigate, Outlet } from "react-router-dom";

import { menu } from "./menu";
import { hasChildren } from "./utils";
import { getUser } from "../../utils/helpers";
import { PATH } from "../../utils/path";

const drawerWidth = 250;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    /*    
    easing:theme.transitions.easing.sharp,
    duration:theme.transitions.duration.enteringScreen
    */
  }),
  overflowX: "hidden",
});
const SmallAvatar = styled(Avatar)(({ theme }) => ({
  width: 22,
  height: 22,
  border: `2px solid ${theme.palette.background.paper}`,
}));

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",

  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

// start new

const MenuItem_Function = ({ item }) => {
  const Component = hasChildren(item) ? MultiLevel : SingleLevel;
  return <Component item={item} />;
};

const SingleLevel = ({ item }) => {
  let navigate = useNavigate();
  return (
    <ListItem
      button
      onClick={() => {
        navigate(item.to, {});
      }}
    >
      <ListItemIcon>{item.icon}</ListItemIcon>
      <ListItemText primary={item.title} />
    </ListItem>
  );
};

const MultiLevel = ({ item }) => {
  const { items: children } = item;
  const [open, setOpen] = useState(false);
  let navigate = useNavigate();

  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  return (
    <>
      <ListItem button onClick={handleClick}>
        <ListItemIcon>{item.icon}</ListItemIcon>
        <ListItemText primary={item.title} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {children.map((child, key) => (
            <MenuItem_Function key={key} item={child} />
          ))}
        </List>
      </Collapse>
    </>
  );
};
// finish new
let tamanioventana = window.screen.width || 0;
export default function MiniDrawer({ sesion, closesesion }) {
  const theme = useTheme();

  const [open, setOpen] = useState(tamanioventana > 897 ? true : false);
  const [quedispositivoes, setQuedispositivoes] = useState(
    tamanioventana > 897 ? "pc" : "phone"
  );
  const [isactiveItem, setIsactiveItem] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const [sesionUser, setSesionUser] = useState([]);
  const [email, setEmail] = useState("");
  const [OpcionSeleccionadoMenu, setOpcionSeleccionadoMenu] = useState(0);
  const [infoUser, setInfoUser] = useState({
    name: "",
    email: "",
    sede: "",
    usuario_id: 0,
  });
  let navigate = useNavigate();

  const handleDrawerOpen = () => {
    if (quedispositivoes === "pc") {
      setOpen(true);
    }
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const open_anchor = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  /**funciones de procesos */

  useEffect(() => {
    getUser((data) => {
      setInfoUser(data);
    });
  }, []);

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar style={{ background: "white" }} position="fixed" open={open}>
        <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="end"
            sx={{
              marginRight: "0px",
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon color="error" />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            <Link
              style={{ textDecoration: "none", color: "red" }}
              to={{ pathname: "/app/parteDiarios" }}
            >
              <Badge
                overlap="rectangular"
                
              >
                <Avatar variant="rounded" src={require("../../sources/iconpedregal.PNG")} />
              </Badge>
              <Box component={"label"} sx={{ ml: 1,fontFamily:'Bodoni MT Poster Compressed' }}>
                Pedregal
              </Box>
            </Link>
          </Typography>
          {sesion ? (
            <>
              <Tooltip title="Opciones">
                <IconButton
                  onClick={handleClick}
                  size="large"
                  sx={{ ml: 2 }}
                  aria-controls={open_anchor ? "account-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={open_anchor ? "true" : undefined}
                >
                  {
                    <Avatar sx={{ width: 32, height: 32 }}>
                      {infoUser.name.substr(0, 1)}
                    </Avatar>
                  }
                </IconButton>
              </Tooltip>
              <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open_anchor}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                  elevation: 0,
                  sx: {
                    overflow: "visible",
                    filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                    mt: 1.5,
                    "& .MuiAvatar-root": {
                      width: 32,
                      height: 32,
                      ml: -0.5,
                      mr: 1,
                    },
                    "&:before": {
                      content: '""',
                      display: "block",
                      position: "absolute",
                      top: 0,
                      right: 14,
                      width: 10,
                      height: 10,
                      bgcolor: "background.paper",
                      transform: "translateY(-50%) rotate(45deg)",
                      zIndex: 0,
                    },
                  },
                }}
                transformOrigin={{ horizontal: "right", vertical: "top" }}
                anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
              >
                <MenuItem>
                  <Avatar />

                  {infoUser.name}
                </MenuItem>

                <Divider />

                <MenuItem
                  onClick={() => {
                    closesesion(false);
                  }}
                >
                  <ListItemIcon>
                    <Logout fontSize="small" />
                  </ListItemIcon>
                  Cerrar Sesión
                </MenuItem>
              </Menu>
            </>
          ) : (
            void 0
          )}
        </Toolbar>
      </AppBar>
      {quedispositivoes === "pc" ? (
        <Drawer variant="permanent" open={open}>
          <DrawerHeader>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "rtl" ? (
                <ChevronRightIcon />
              ) : (
                <ChevronLeftIcon />
              )}
            </IconButton>
          </DrawerHeader>
          <Divider />
          <List>
            {menu.map((item, key) => (
              <MenuItem_Function key={key} item={item} />
            ))}
          </List>
        </Drawer>
      ) : quedispositivoes === "phone" ? (
        <Paper
          sx={{
            position: "fixed",
            bottom: 0,
            left: 0,
            right: 0,
            background: "white",
            zIndex: 1,
          }}
          elevation={9}
        >
          <BottomNavigation
            showLabels
            value={OpcionSeleccionadoMenu}
            onChange={(event, newValue) => {
              setOpcionSeleccionadoMenu(newValue);
            }}
          >
            <BottomNavigationAction
              onClick={() => {
                navigate(PATH.PARTEDIARIO, {});
              }}
              label={`Parte Diario`}
              icon={<AutoStoriesIcon />}
            />
            <BottomNavigationAction
              onClick={() => {
                navigate(PATH.CREARPARTEDIARIO, {});
              }}
              label="Crear Parte Diario"
              icon={<AppRegistrationIcon />}
            />
            <BottomNavigationAction
              onClick={() => {
                navigate(PATH.SINCRONIZARCOLECCION, {});
              }}
              label="Sincronizar"
              icon={<CloudSyncIcon />}
            />
          </BottomNavigation>
        </Paper>
      ) : (
        void 0
      )}

      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />

        <Outlet />
      </Box>
    </Box>
  );
}
