import AppsIcon from '@mui/icons-material/Apps';
import AdUnitsIcon from '@mui/icons-material/AdUnits';
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import ContentPasteGoIcon from '@mui/icons-material/ContentPasteGo';
import TravelExploreIcon from '@mui/icons-material/TravelExplore';
import ListAltIcon from '@mui/icons-material/ListAlt'
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import SplitscreenIcon from '@mui/icons-material/Splitscreen';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import PersonPinCircleIcon from '@mui/icons-material/PersonPinCircle';
import DashboardCustomizeIcon from '@mui/icons-material/DashboardCustomize';
import PolicyIcon from '@mui/icons-material/Policy';
import CloudSyncIcon from '@mui/icons-material/CloudSync';
import CachedIcon from '@mui/icons-material/Cached';
export const menu = [
   {
      title:'Parte Diario',
      to:'',
      icon:<AdUnitsIcon />,
      items:[
         
         {
            title:'Parte Diarios',
            to:'parteDiarios', 
            icon:<AutoStoriesIcon/>
         },
         {
            title:'Crear Parte Diario',
            to:'crearParteDiario', 
            icon:<AppRegistrationIcon/>
         },
         /**
          {
            title:'Consultar Personal',
            to:'personalporpartediario', 
            icon:<SplitscreenIcon/>
         },
          */
      ]
     
  },
  {
   title:'Maestros',
   to:'',
   icon:<DashboardCustomizeIcon />,
   items:[
      
      
      {
         title:'Personal',
         to:'personal', 
         icon:<PersonPinCircleIcon/>
      },
      {
         title:'Usuarios',
         to:'usuarios', 
         icon:<PolicyIcon/>
      },
   ]
  
},
{
   title:'Sincronizar',
   to:'',
   icon:<CloudSyncIcon />,
   items:[
      
      
      {
         title:'Actualizar Cache',
         to:'sincronizarcoleccion', 
         icon:<CachedIcon/>
      }
   ]
  
},
    /*
    {
        title:'Demo',
        to:'',
        icon:<AppsIcon />,
        items:[
            {
               title:'Usuarios',
               to:'usuarios', 
               icon:<QueryStatsIcon />
            },
            {
                title:'Transferidos',
                to:'usuariosagregados', 
                icon:<ContentPasteGoIcon/>
             },
             {
                title:'Campo',
                to:'campo', 
                icon:<TravelExploreIcon/>
             }
        ]
    }
    */
   
    
    
];
