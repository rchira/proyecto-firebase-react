import { CircularProgress, Autocomplete, TextField } from "@mui/material";
import { Fragment, useEffect, useState } from "react";

function sleep(delay = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}

export default function Asynchronous({
  data,
  getSelected,
  label,
  Namelabel,
  IdLabel,
  valueInput = "",
  messageError = "",
  isDisabled = false,
}) {
  console.log(valueInput);
  const [open, setOpen] = useState(false);
  const [labores, setLabores] = useState([]);
  const loading = open && labores.length === 0;



  useEffect(() => {
    setLabores(data);

    /**
     let active = true;
    if (!loading) {
      return undefined;
    }

    (async () => {
   
     
      if (active) {
        setLabores([...topFilms]);
      }
    })();

    return () => {
      active = false;
    };
   */
  }, [loading, data]);

  useEffect(() => {
    /**
     if (!open) {
      setLabores([]);
    }
     */
  }, [open]);

  return (
    <Autocomplete
      disabled={isDisabled}
      id="asynchronous-demo"
      //   sx={{ width:  }}
      open={open}
      onChange={(event, newValue) => {
        
        getSelected(
          { [Namelabel]: newValue[Namelabel], [IdLabel]: newValue[IdLabel] },
          newValue
        );
      }}
     
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      //   isOptionEqualToValue={(option, value) => option.labor === value.labor_id}
      getOptionLabel={(option, i) =>
        // if(i==0){
        //   console.log(i);
        // setDefaultValue(option[0][Namelabel])}
        option[Namelabel]
      }
      options={labores}
      loading={loading}
      renderInput={(params, i) => (
        <TextField
          error={messageError && messageError.length > 0 ? true : false}
          helperText={messageError}
          key={i}
          {...params}
          label={`${label}`}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <Fragment>
                {loading ? <CircularProgress color="error" size={20} /> : null}
                {params.InputProps.endAdornment}
              </Fragment>
            ),
          }}
        />
      )}
    />
  );
}
