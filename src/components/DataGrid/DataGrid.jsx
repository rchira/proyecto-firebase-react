import * as React from 'react';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import { Box } from '@mui/material';




export default function DataGridDemo({data=[],keyRow,configColumns=[],
  onRowDoubleClick,onCellEditCommit,onCellClick} ) {
  return (
    <div style={{ height: 600, width: '100%' }}>
      <DataGrid
       onRowDoubleClick={onRowDoubleClick}
      //  components={{ Toolbar: GridToolbar }}
       componentsProps={{ toolbar: { csvOptions: { allColumns: true } } }}
      
        getRowId={(row) => row[keyRow]}
        rows={data}
        columns={configColumns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        // checkboxSelection
        disableSelectionOnClick
        onCellEditCommit={onCellEditCommit}
        onCellClick={onCellClick}
     sx={{mb:5}}
      />
   <br/>
    </div>
  );
}
