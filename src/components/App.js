import {
  BrowserRouter as Router,
  Route,
  Routes,
  useRoutes,
  Navigate,
} from "react-router-dom";
import Campo from "../pages/campo/campo";
import { Cpanel, ListPersonalPD } from "../pages/pd/ListPersonalpd";
import SignInUser from "../pages/login/user";
import ListUsers from "../pages/users/ListUsers";
import UserAgregados from "../pages/users/userAgregados";
import { DetailsPersonal } from "../pages/Person/detailsPersonal";
import MiniDrawer from "./Layout/Layout";
import Crearpd from "../pages/pd/Crearpd";
import { useEffect, useState } from "react";
import { auth } from "../server/coneccion/config";
import {ParteDiarios} from "../pages/pd/ParteDiarios";
import { Asitencia } from "../pages/pd/asistencia";
import FadingLoader from "./ContentLoader/FadingLoader";
import { Sincronizacion } from "../pages/sincronizacion";
import { ListarPersonal } from "../pages/maestros/ListarPersonal";
import { ListarUsuarios } from "../pages/maestros/ListarUsuarios";
import metodos from "../server/metodos";
import { alNavegador } from "../utils/helpers";
import { RegistrarRendimiento } from "../pages/pd/RegistrarRendimiento";
import { PATH } from "../utils/path";
import { ActualizarCache } from "../pages/sincronizacion/actualizarCache";
import "react-confirm-alert/src/react-confirm-alert.css";

function App() {
  const [isLogged, setIsLogged] = useState(false);
  const [isConnected, setConnected] = useState(false);
  const [estaSincronizado, setEstaSincronizado] = useState(false);

  useEffect(async () => {
     let {status,user}= await  metodos.usuarios.estaAutentificadoModel()
    if(status){
      console.log(user);
      setIsLogged(true);
      setConnected(true);
    }else{
      setIsLogged(false);
      setConnected(true);
      localStorage.removeItem("user");
    }
  }, []);

 

  function EvaluatedSesionComponent(component,contentLoader){
    if(isConnected){
      if(isLogged){
        return component
      }else{
       return <Navigate to="/login"  />
      }
    }else{
      // https://skeletonreact.com/#gallery
     return contentLoader
    }
  }

  let element = useRoutes([
    { path: "/", element: <Navigate to="/app/parteDiarios" /> },
 
    {
      path: "/login",
      element: !isLogged ? (
        <SignInUser
          
        />
      ) : (
        <Navigate to="/app/parteDiarios" />
      ),
    },
    
    { path: "/sincronizacion", element: <Sincronizacion
  
      handleActiveLogin={(e) => {
        setConnected(true);
        setIsLogged(e);

     }}

    
    /> },
    {
      path: "app",
      element: (
        <MiniDrawer
          sesion={isLogged}
          closesesion={(e) => {
            if (auth.signOut()) {
              setIsLogged(false)
            }
          }}
        />
      ),
      children: [
        {
          path: PATH.REGISTRARRENDIMIENTO,
          element: EvaluatedSesionComponent(<RegistrarRendimiento />,<FadingLoader />)
        },
        {
          path: "/app/parteDiarios",
          element: EvaluatedSesionComponent(<ParteDiarios />,<FadingLoader />)
        },
        {
          path: "/app/usuariosagregados",
          element: EvaluatedSesionComponent(<UserAgregados />)
        },
        {
          path: "/app/campo",
          element:EvaluatedSesionComponent(<Campo />),
        },
       /*
        {
          path: "/app/usuarios",
          element:EvaluatedSesionComponent(<ListUsers />),
        },
       */
        {
          path: "/app/personalporpartediario",
          element:EvaluatedSesionComponent(<ListPersonalPD />) ,
        },
        {
          path: "/app/detallerendimientopersonal",
          element:EvaluatedSesionComponent(<DetailsPersonal /> ),
        },
        {
          path: "/app/crearParteDiario",
          element: EvaluatedSesionComponent(<Crearpd />,<FadingLoader />),
        },
        {
          path: "/app/asistencia",
          element: EvaluatedSesionComponent(<Asitencia />),
        },{
          path: "/app/personal",
          element: EvaluatedSesionComponent(<ListarPersonal />),
        }
        ,{
          path: "/app/usuarios",
          element: EvaluatedSesionComponent(<ListarUsuarios />),
        },
        ,{
          path: "/app/sincronizarcoleccion",
          element: EvaluatedSesionComponent(<ActualizarCache />),
        },
      ],
    },
  ]);

  return element;
}

export default App;
