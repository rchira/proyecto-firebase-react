
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
  import { Line } from 'react-chartjs-2';
  import faker from 'faker';
  
  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );
  
  

export function Chart({titleChar,labels,dataset=[]}) {

   console.log(labels);

    // start char
     const options = {
        responsive: true,
        plugins: {
          legend: {
            position: 'left'
          },
          title: {
            display: true,
            text: titleChar,
          },
        },
      };
      
  
      
       const data = {
        labels,
        datasets:dataset,
      };
    
    // finish char
    return (<>
        <>

            <Line options={options} data={data} />
        </>
        {/* https://www.npmjs.com/package/react-chartjs-2 */}
    </>)
}