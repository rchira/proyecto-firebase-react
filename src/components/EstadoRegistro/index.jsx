
import Tooltip, { tooltipClasses } from "@mui/material/Tooltip";


import CloudOutlinedIcon from '@mui/icons-material/CloudOutlined';
import CloudOffOutlinedIcon from '@mui/icons-material/CloudOffOutlined';
import { styled } from "@mui/material";

const LightTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
  },
}));

export function EstadoRegistro({ fecha,sx }) {
  var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', };
  return (
    <>
      {(fecha && fecha.toDate()) ? (
        <LightTooltip  title={`Se registro en Firebase el ${fecha.toDate().toLocaleTimeString("es-ES", options)}`}>
          <CloudOutlinedIcon    color="success" sx={{ ...sx }} />
        </LightTooltip>
      ) : (
        <LightTooltip  title={`El registro aun no esta en Firebase`}>
          <CloudOffOutlinedIcon   color="warning" sx={{ ...sx }} />
        </LightTooltip>
      )}
    </>
  );
}
